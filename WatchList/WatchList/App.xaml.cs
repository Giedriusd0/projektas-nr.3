﻿using System.Threading;
using System.Windows;

namespace WatchList
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            API.Client.Client.LoadConfig();
        }
    }
}
