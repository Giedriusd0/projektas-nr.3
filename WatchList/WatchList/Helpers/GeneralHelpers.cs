﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using WatchList.API.Configurations;

namespace WatchList.Helpers
{
    class GeneralHelpers
    {
        internal enum FolderTypes { Auth, Posters, Cache }

        internal static void DelayAction(int miliseconds, Action action)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                DispatcherTimer timer = new DispatcherTimer();
                timer.Tick += delegate
                {
                    action.Invoke();
                    timer.Stop();
                    timer = null;
                };
                timer.Interval = TimeSpan.FromMilliseconds(miliseconds);
                timer.Start();
            });
        }

        internal static string GetFolder(FolderTypes type)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\WatchList\\";
            switch (type)
            {
                case FolderTypes.Auth: path += "OAuth"; break;
                case FolderTypes.Posters: path += "Posters"; break;
                case FolderTypes.Cache: path += "Cache"; break;
            }
            Directory.CreateDirectory(path);
            return path + "\\";
        }

        internal static BitmapImage GetUnlockedImage(string path, double width)
        {
            BitmapImage bitmap = new BitmapImage();
            try
            {
                using (Stream stream = File.OpenRead(path))
                {
                    stream.Position = 0;
                    bitmap.BeginInit();
                    bitmap.StreamSource = stream;
                    bitmap.DecodePixelWidth = (int)width;
                    bitmap.CacheOption = BitmapCacheOption.OnLoad;
                    bitmap.EndInit();
                    RenderOptions.SetBitmapScalingMode(bitmap, BitmapScalingMode.Linear);
                    RenderOptions.SetCachingHint(bitmap, CachingHint.Unspecified);
                    bitmap.Freeze();
                }
            }
            catch
            {
                File.Delete(path);
                string[] paths = path.Split('\\');
                bitmap = GetUnlockedImage(API.Images.LoadPoster("/" + paths.Last()), width);
            }
            return bitmap;
        }
        internal static List<string> GenerateYears(int Amount)
        {
            DateTime CurrentTime = DateTime.Now;
            List<string> Years = new List<string>() { "None" };

            for (int i = 0; i <= Amount; i++)
            {
                Years.Add((CurrentTime.Year - i).ToString());
            }
            return Years;
        }
        internal static ObservableCollection<string> ToStringColl(List<Models.General.Genre> list)
        {
            ObservableCollection<string> coll = new ObservableCollection<string>();
            foreach (var genre in list)
            {
                coll.Add(genre.Name.ToString());
            }
            return coll;
        }
    }
}
