﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using WatchList.Models.Search;

namespace WatchList.Helpers
{
    public class HomeControlDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate MovieTemplate { get; set; }
        public DataTemplate TvShowTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item != null)
            {
                if (item.GetType() == typeof(SearchMovie))
                {
                    return MovieTemplate;
                }
                if (item.GetType() == typeof(SearchTv))
                {
                    return TvShowTemplate;
                }
            }
            return null;
        }

    }
}
