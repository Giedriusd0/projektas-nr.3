﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using WatchList.Models.Search;

namespace WatchList.Helpers
{
    public class ListControlDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate MovieTemplate { get; set; }
        public DataTemplate TvShowTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item == null)
                return null;
            else
            {
                if (item.GetType() == typeof(Models.Movies.Movie))
                {
                    return MovieTemplate;
                }
                if (item.GetType() == typeof(Models.TvShows.TvShow))
                {
                    return TvShowTemplate;
                }
            }
            
            return null;
        }

    }
}
