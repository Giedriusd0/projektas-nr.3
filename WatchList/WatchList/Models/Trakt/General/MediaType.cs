﻿using WatchList.Helpers;

namespace WatchList.Models.Trakt.General
{
    public enum MediaType
    {
        [EnumValue("movie")]
        Movie,
        [EnumValue("show")]
        Show
    }
}
