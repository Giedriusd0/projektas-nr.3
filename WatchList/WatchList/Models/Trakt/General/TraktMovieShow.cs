﻿using Newtonsoft.Json;

namespace WatchList.Models.Trakt.General
{
    public class TraktMovieShow
    {
        public MediaType Type { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("year")]
        public int Year { get; set; }
        [JsonProperty("ids")]
        public TraktIds Ids { get; set; }
    }
}
