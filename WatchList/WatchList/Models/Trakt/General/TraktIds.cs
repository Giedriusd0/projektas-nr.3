﻿using Newtonsoft.Json;

namespace WatchList.Models.Trakt.General
{
    public class TraktIds
    {
        [JsonProperty("slug")]
        public string Slug { get; set; }
        [JsonProperty("trakt")]
        public string Trakt { get; set; }
        [JsonProperty("tvdb")]
        public string Tvdb { get; set; }
        [JsonProperty("imdb")]
        public string Imdb { get; set; }
        [JsonProperty("tmdb")]
        public string Tmdb { get; set; }
    }
}
