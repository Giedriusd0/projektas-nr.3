﻿using Newtonsoft.Json;
using WatchList.Models.Trakt.General;

namespace WatchList.Models.Trakt.TvShows
{
    public class TraktEpisode
    {
        [JsonProperty("season")]
        public int Season { get; set; }
        [JsonProperty("number")]
        public int Number { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("ids")]
        public TraktIds Ids { get; set; }

    }
}
