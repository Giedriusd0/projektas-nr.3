﻿using Newtonsoft.Json;
using System;
using WatchList.Models.Trakt.General;

namespace WatchList.Models.Trakt.Calendar
{
    public class Movies : ICalendar
    {
        [JsonProperty("released")]
        public DateTime? Released { get; set; }
        [JsonProperty("movie")]
        public TraktMovieShow Movie { get; set; }

        public string Title => Movie.Title;
        public MediaType Type => MediaType.Movie;
        public string TraktId => Movie.Ids.Trakt;
    }
}
