﻿using Newtonsoft.Json;
using System;
using WatchList.Models.Trakt.General;
using WatchList.Models.Trakt.TvShows;

namespace WatchList.Models.Trakt.Calendar
{
    public class TvShow : ICalendar
    {
        [JsonProperty("First_aired")]
        public DateTime? FirstAired { get; set; }
        [JsonProperty("episode")]
        public TraktEpisode Episode { get; set; }
        [JsonProperty("show")]
        public TraktMovieShow Show { get; set; }

        public MediaType Type => MediaType.Show;
        public string TraktId => Episode.Ids.Trakt;
        public string Title => Show.Title;
        public DateTime? Released => FirstAired;
    }
}
