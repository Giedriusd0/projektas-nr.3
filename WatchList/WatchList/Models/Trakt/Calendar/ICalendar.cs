﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatchList.Models.Trakt.General;

namespace WatchList.Models.Trakt.Calendar
{
    public interface ICalendar
    {
        string TraktId { get; }
        string Title { get; }
        DateTime? Released { get; }
        MediaType Type { get; }
    }
}
