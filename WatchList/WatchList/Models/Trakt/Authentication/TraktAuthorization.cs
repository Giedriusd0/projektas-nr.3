﻿using Newtonsoft.Json;
using ProtoBuf;

namespace WatchList.Models.Trakt.Authentication
{
    [ProtoContract]
    public class TraktAuthorization
        {
        [ProtoMember(1)]
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [ProtoMember(2)]
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        //[JsonProperty("scope")]
        //[JsonConverter(typeof(TraktEnumerationConverter<TraktAccessScope>))]
        //public TraktAccessScope AccessScope { get; set; }
        [ProtoMember(3)]
        [JsonProperty("expires_in")]
        public int ExpiresInSeconds { get; set; }
        //[JsonProperty(PropertyName = "token_type")]
        //[JsonConverter(typeof(TraktEnumerationConverter<TraktAccessTokenType>))]
        //public TraktAccessTokenType TokenType { get; set; }
    }
}
