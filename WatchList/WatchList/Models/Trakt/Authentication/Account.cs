﻿using Newtonsoft.Json;

namespace WatchList.Models.Trakt.Authentication
{
    public class Account
    {
        [JsonProperty("timezone")]
        public string TimeZone { get; set; }

        [JsonProperty("time_24hr")]
        public bool Time_24hr { get;set; }
    }
}
