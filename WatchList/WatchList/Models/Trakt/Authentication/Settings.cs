﻿using Newtonsoft.Json;

namespace WatchList.Models.Trakt.Authentication
{
    public class Settings
    {
        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("account")]
        public Account Account { get; set; }

    }
}
