﻿using Newtonsoft.Json;
using System;
using WatchList.Models.Trakt.General;

namespace WatchList.Models.Trakt.Authentication
{
    public class User
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("private")]
        public bool Private { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("vip")]
        public bool Vip { get; set; }

        [JsonProperty("ids")]
        public TraktIds Ids { get; set; }

        [JsonProperty("joined_at")]
        public DateTime JoinedAt { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("about")]
        public string About { get; set; }

        [JsonProperty("genre")]
        public string Genre { get; set; }

        [JsonProperty("age")]
        public int? Age { get; set; }
    }
}
