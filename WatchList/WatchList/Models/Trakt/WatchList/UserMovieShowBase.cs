﻿using Newtonsoft.Json;
using System;
using WatchList.Models.Trakt.General;

namespace WatchList.Models.Trakt.WatchList
{
    public class UserMovieShow
    {
        [JsonProperty("listed_at")]
        public DateTime ListedAt { get; set; }

        public TraktMovieShow MovieShow { get; set; }

        public int TmdbId => Int32.Parse(MovieShow.Ids.Tmdb);
    }
}
