﻿using Newtonsoft.Json;

namespace WatchList.Models.Trakt.WatchList
{
    public class WatchListResults
    {
        [JsonProperty("added")]
        public TraktMediaTypes Added { get; set; }

        [JsonProperty("deleted")]
        public TraktMediaTypes Deleted { get; set; }
    }

    public partial class TraktMediaTypes
    {
        [JsonProperty("movies")]
        public long Movies { get; set; }

        [JsonProperty("shows")]
        public long Shows { get; set; }

        [JsonProperty("seasons")]
        public long Seasons { get; set; }

        [JsonProperty("episodes")]
        public long Episodes { get; set; }
    }
}
