﻿using Newtonsoft.Json;
using System.Collections.Generic;
using WatchList.Models.General;

namespace WatchList.Models.Movies
{
    public class Credits
    {
        [JsonProperty("cast")]
        public List<Cast> Cast { get; set; }

        [JsonProperty("crew")]
        public List<Crew> Crew { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
