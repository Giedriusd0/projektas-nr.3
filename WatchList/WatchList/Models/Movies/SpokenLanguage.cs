﻿using Newtonsoft.Json;

namespace WatchList.Models.Movies
{
    public class SpokenLanguage
    {
        /// <summary>
        /// A language code, e.g. en
        /// </summary>
        [JsonProperty("iso_639_1")]
        public string Iso_639_1 { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
