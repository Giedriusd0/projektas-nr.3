﻿using Newtonsoft.Json;

namespace WatchList.Models.Movies
{
    public class ProductionCountry
    {
        /// <summary>
        /// A country code, e.g. US
        /// </summary>
        [JsonProperty("iso_3166_1")]
        public string Iso_3166_1 { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
