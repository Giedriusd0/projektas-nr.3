﻿using Newtonsoft.Json;
using System.Collections.Generic;
using WatchList.Models.General;

namespace WatchList.Models.Movies
{
    public class AlternativeTitles
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("titles")]
        public List<AlternativeTitle> Titles { get; set; }
    }
}
