﻿using Newtonsoft.Json;
using System.Collections.Generic;
using WatchList.Models.General;

namespace WatchList.Models.Movies
{
    public class KeywordsContainer
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("keywords")]
        public List<Keyword> Keywords { get; set; }
    }
}
