﻿using Newtonsoft.Json;

namespace WatchList.Models.Movies
{
    public class ProductionCompany
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
