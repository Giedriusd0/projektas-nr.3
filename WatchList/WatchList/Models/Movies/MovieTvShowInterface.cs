﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatchList.Models.General;

namespace WatchList.Models.Movies
{
    public interface IMovieTvShow
    {
        int Id { get; set; }
        DateTime? ReleaseDate { get; }
        MediaType MediaType { get; }
        string Title { get; }
    }
}
