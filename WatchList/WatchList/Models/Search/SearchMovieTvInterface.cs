﻿using WatchList.Models.General;

namespace WatchList.Models.Search
{
    public interface ISearchMovieTvInterface
    {
        int Id { get; set; }
        MediaType MediaType { get; set; }
        bool IsInWatchList { get; set; }
    }
}
