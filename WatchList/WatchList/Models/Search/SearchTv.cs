﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatchList.Models.General;

namespace WatchList.Models.Search
{
    public class SearchTv : SearchMovieTvBase, ISearchMovieTvInterface
    {
        public SearchTv()
        {
            MediaType = MediaType.Tv;
        }
        [JsonProperty("first_air_date")]
        public DateTime? FirstAirDate { get; set; }

        public string Title { get { return Name; } }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("original_name")]
        public string OriginalName { get; set; }

        /// <summary>
        /// Country ISO code ex. US
        /// </summary>
        [JsonProperty("origin_country")]
        public List<string> OriginCountry { get; set; }

        public SearchTv This { get { return this; } }
    }
}
