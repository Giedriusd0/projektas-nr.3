﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using WatchList.Models.General;

namespace WatchList.Models.Search
{
    public class KnownForTv : KnownForBase
    {
        public KnownForTv()
        {
            MediaType = MediaType.Tv;
        }

        [JsonProperty("first_air_date")]
        public DateTime? FirstAirDate { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("original_name")]
        public string OriginalName { get; set; }

        [JsonProperty("origin_country")]
        public List<string> OriginCountry { get; set; }
    }
}
