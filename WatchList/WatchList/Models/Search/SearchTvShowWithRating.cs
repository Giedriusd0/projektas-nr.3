﻿using Newtonsoft.Json;

namespace WatchList.Models.Search
{
    public class SearchTvShowWithRating : SearchTv
    {
        [JsonProperty("rating")]
        public double Rating { get; set; }
    }
}
