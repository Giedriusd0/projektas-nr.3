﻿using Newtonsoft.Json;

namespace WatchList.Models.Search
{
    public class AccountSearchTv : SearchTv
    {
        [JsonProperty("rating")]
        public float Rating { get; set; }
    }
}
