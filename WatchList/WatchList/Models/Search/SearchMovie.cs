﻿using Newtonsoft.Json;
using System;
using WatchList.Models.General;

namespace WatchList.Models.Search
{
    public class SearchMovie : SearchMovieTvBase, ISearchMovieTvInterface
    {
        public SearchMovie()
        {
            MediaType = MediaType.Movie;
        }

        [JsonProperty("adult")]
        public bool Adult { get; set; }

        [JsonProperty("original_title")]
        public string OriginalTitle { get; set; }

        [JsonProperty("release_date")]
        public DateTime? ReleaseDate { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("video")]
        public bool Video { get; set; }

        public SearchMovie This { get { return this; } }
    }
}
