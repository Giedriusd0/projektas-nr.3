﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WatchList.Models.Search
{
    public class SearchMovieTvBase : SearchBase
    {
        [JsonProperty("backdrop_path")]
        public string BackdropPath { get; set; }

        [JsonProperty("genre_ids")]
        public List<int> GenreIds { set
            {
                Genres = API.Configurations.Genre.ConvertFromGenreIds(value);
            }
        }

        public List<string> Genres { get; private set; }

        [JsonProperty("original_language")]
        public string OriginalLanguage { get; set; }

        [JsonProperty("overview")]
        public string Overview { get; set; }

        [JsonProperty("poster_path")]
        public string PosterPath { get; set; }

        [JsonProperty("vote_average")]
        public double VoteAverage { get; set; }

        [JsonProperty("vote_count")]
        public int VoteCount { get; set; }

        public bool IsInWatchList { get; set; }

    }
}
