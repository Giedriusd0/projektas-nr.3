﻿using Newtonsoft.Json;

namespace WatchList.Models.Search
{
    public class SearchMovieWithRating : SearchMovie
    {
        [JsonProperty("rating")]
        public double Rating { get; set; }
    }
}
