﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WatchList.Models.TvShows
{
    public class OnTheAirTvShow
    {
        public string TvShowName { get; set; }

        public int TvShowId { get; set; }
        
        public int SeasonNumber { get; set; }

        public string PosterPath { get; set; }

        public int EpisodeNumber { get; set; }

        public DateTime? AirDate { get; set; }
    }
}
