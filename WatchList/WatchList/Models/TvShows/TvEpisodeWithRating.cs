﻿using Newtonsoft.Json;

namespace WatchList.Models.TvShows
{
    public class TvEpisodeWithRating : TvEpisode
    {
        [JsonProperty("rating")]
        public double Rating { get; set; }
    }
}