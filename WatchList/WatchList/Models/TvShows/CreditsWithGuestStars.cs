﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace WatchList.Models.TvShows
{
    public class CreditsWithGuestStars : Credits
    {
        [JsonProperty("guest_stars")]
        public List<Cast> GuestStars { get; set; }
    }
}