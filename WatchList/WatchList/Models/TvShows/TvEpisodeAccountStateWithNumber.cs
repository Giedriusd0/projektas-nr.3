using Newtonsoft.Json;

namespace WatchList.Models.TvShows
{
    public class TvEpisodeAccountStateWithNumber : TvEpisodeAccountState
    {
        [JsonProperty("episode_number")]
        public int EpisodeNumber { get; set; }
    }
}