﻿using Newtonsoft.Json;

namespace WatchList.Models.TvShows
{
    public class TvAccountState
    {
        [JsonProperty("rating")]
        public double? Rating { get; set; }
    }
}