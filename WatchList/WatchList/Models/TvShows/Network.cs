﻿using Newtonsoft.Json;

namespace WatchList.Models.TvShows
{
    public class Network
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
