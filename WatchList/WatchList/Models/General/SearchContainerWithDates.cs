﻿using Newtonsoft.Json;

namespace WatchList.Models.General
{
    public class SearchContainerWithDates<T> : SearchContainer<T>
    {
        [JsonProperty("dates")]
        public DateRange Dates { get; set; }
    }
}
