﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WatchList.Models.General
{
    public class Certification
    {
        [JsonProperty("certifications")]
        public CountryCertifications Certifications { get; set; }
    }

    public class CountryCertifications
    {
        [JsonProperty("US")]
        public List<SubCertification> US { get; set; }
        [JsonProperty("CA")]
        public List<SubCertification> CA { get; set; }
        [JsonProperty("AU")]
        public List<SubCertification> AU { get; set; }
        [JsonProperty("FR")]
        public List<SubCertification> FR { get; set; }
        [JsonProperty("DE")]
        public List<SubCertification> DE { get; set; }
        [JsonProperty("GB")]
        public List<SubCertification> GB { get; set; }
        [JsonProperty("BR")]
        public List<SubCertification> BR { get; set; }
        [JsonProperty("RU")]
        public List<SubCertification> RU { get; set; }
        [JsonProperty("TH")]
        public List<SubCertification> TH { get; set; }
        [JsonProperty("KR")]
        public List<SubCertification> KR { get; set; }
        [JsonProperty("NZ")]
        public List<SubCertification> NZ { get; set; }
        [JsonProperty("IN")]
        public List<SubCertification> IN { get; set; }
    }
    
    public class SubCertification
    {
        [JsonProperty("certification")]
        public string Certification { get; set; }
        [JsonProperty("meaning")]
        public string Meaning { get; set; }
        [JsonProperty("order")]
        public int order { get; set; }
    }
}
