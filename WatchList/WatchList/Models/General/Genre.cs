﻿using Newtonsoft.Json;
using ProtoBuf;

namespace WatchList.Models.General
{
    [ProtoContract]
    public class Genre
    {
        [ProtoMember(1)]
        [JsonProperty("id")]
        public int Id { get; set; }
        [ProtoMember(2)]
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
