﻿using Newtonsoft.Json;

namespace WatchList.Models.General
{
    public class ImagesWithId : Images
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
