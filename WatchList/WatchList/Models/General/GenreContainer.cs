﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WatchList.Models.General
{
    public class GenreContainer
    {
        [JsonProperty("genres")]
        public List<Genre> Genres { get; set; }
    }
}
