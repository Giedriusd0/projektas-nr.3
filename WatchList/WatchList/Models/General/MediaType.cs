﻿using Newtonsoft.Json;
using WatchList.Converters;
using WatchList.Helpers;

namespace WatchList.Models.General
{
    [JsonConverter(typeof(EnumStringValueConverter))]
    public enum MediaType
    {
        Unknown,

        [EnumValue("movie")]
        Movie = 1,

        [EnumValue("tv")]
        Tv = 2,

        [EnumValue("person")]
        Person = 3
    }
}
