﻿using System.Collections.Generic;
using WatchList.Models.Movies;
using WatchList.ViewModels;

namespace WatchList.Models.General
{
    public class Day : BaseModel
    {
        public int Number { get; set; }
        public List<IMovieTvShow> Events { get; set; }

        public Day(int number)
        {
            Number = number;
        }
    }
}
