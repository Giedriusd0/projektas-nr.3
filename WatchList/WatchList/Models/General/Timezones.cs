﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WatchList.Models.General
{
    public class Timezones
    {
        [JsonProperty("list")]
        public Dictionary<string, List<string>> List { get; set; }
    }
}
