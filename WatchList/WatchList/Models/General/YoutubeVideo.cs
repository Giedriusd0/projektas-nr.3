﻿namespace WatchList.Models.General
{
    public class YoutubeVideo
    {
        public string Title { get; }
        public string Url { get; }
        public string Quality { get; set; }

        public YoutubeVideo(string title, string url)
        {
            Title = title;
            Url = url;
        }
    }
}
