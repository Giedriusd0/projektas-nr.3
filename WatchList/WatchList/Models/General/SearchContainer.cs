﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using WatchList.Models.Search;

namespace WatchList.Models.General
{
    public class SearchContainer<T>
    {
        public string QueryText { get; set; }

        [JsonProperty("page")]
        public int Page { get; set; }

        [JsonProperty("results")]
        public List<T> Results { get; set; }

        [JsonProperty("total_pages")]
        public int TotalPages { get; set; }

        [JsonProperty("total_results")]
        public int TotalResults { get; set; }

    }
}
