﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WatchList.Models.General
{
    public class TranslationsContainerTv
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("translations")]
        public List<Translation> Translations { get; set; }
    }
}
