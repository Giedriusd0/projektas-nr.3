﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WatchList.Extensions;
using WatchList.Helpers;
using WatchList.Models.Trakt.WatchList;
using WatchList.Models.Trakt.Authentication;
using WatchList.Models.Trakt.General;
using WatchList.API.Client;
using WatchList.Models.Trakt.Calendar;
using Newtonsoft.Json.Linq;
using System;
using System.Threading;

namespace WatchList.API
{
    internal static class Account
    {
        private enum Type
        {
            [EnumValue("shows")]
            Shows,
            [EnumValue("movies")]
            Movies,
        }

        public delegate void EventHandler(Object sender, WatchListArgs e);
        public static event EventHandler WatchListChanged;

        /// <summary>
        /// Already loaded UserWatchList
        /// </summary>
        internal static List<UserMovieShow> WatchList { get; set; }
        internal static List<int> WatchListID { get; set; }
        /// <summary>
        /// Checks if Movie or TvShow exist in current WatchList
        /// </summary>
        /// <param name="id">Tmdb Movie or TvShow id</param>
        /// <returns>exist or not</returns>
        internal static bool IsInWatchList(int id)
        {
            SpinWait.SpinUntil(() => WatchListLoaded);
            if (WatchList == null)
                return false;
            return WatchList.Exists(x => x.TmdbId == id);
        }

        /// <summary>
        /// Gets Current User Settings and information.
        /// </summary>
        /// <returns>User settings</returns>
        internal async static Task<Settings> GetUserSettings()
        {
            Request req = Client.Client.Instance.Create(Request.Service.Trakt, "users/settings");
            req.AddTraktLoginRequired();
            return await req.ExecuteGet<Settings>().ConfigureAwait(false);
        }

        internal static bool WatchListLoaded { get; set; }
        /// <summary>
        /// Updates WatchList
        /// </summary>
        /// <returns></returns>
        internal async static Task UpdateWatchList()
        {
            if(!Client.Client.IsLoggedIn)
            {
                WatchListLoaded = true;
                return;
            }
            if (WatchList == null)
                WatchList = new List<UserMovieShow>();
            WatchList.Clear();
            WatchList.AddRange(await GetWatchListMovies());
            WatchList.AddRange(await GetWatchListTvShows());
            UpdateWatchListID();
            WatchListChanged?.Invoke(null, new WatchListArgs(WatchList));
            WatchListLoaded = true;
        }

        private static void UpdateWatchListID()
        {
            if (WatchListID == null)
                WatchListID = new List<int>();
            foreach (var item in WatchList)
            {
                WatchListID.Add(item.TmdbId);
            }
        }

        /// <summary>
        /// Gets current TvShows in WatchList
        /// </summary>
        /// <returns></returns>
        internal async static Task<List<UserMovieShow>> GetWatchListTvShows()
        {
            return await GetWatchList<List<UserMovieShow>>(Type.Shows).ConfigureAwait(false);
        }

        /// <summary>
        /// Gets current movies in WatchList
        /// </summary>
        /// <returns></returns>
        internal async static Task<List<UserMovieShow>> GetWatchListMovies()
        {
            return await GetWatchList<List<UserMovieShow>>(Type.Movies).ConfigureAwait(false);
        }

        private async static Task<T> GetWatchList<T>(Type type)
        {
            Request req = Client.Client.Instance.Create(Request.Service.Trakt, "sync/watchlist");
            req.AddTraktParameter(type.GetDescription());
            req.AddTraktLoginRequired();
            Response<T> response = await req.ExecuteGet<T>().ConfigureAwait(false);
            return response;
        }

        /// <summary>
        /// Adds Movie to WatchList
        /// </summary>
        /// <param name="id">Tmdb Movie id</param>
        /// <returns>Is added</returns>
        internal async static Task<bool> AddMovieToWatchList(int id)
        {
            WatchList.Add(new UserMovieShow() { ListedAt = DateTime.Now, MovieShow = new TraktMovieShow() { Type = MediaType.Movie, Ids = new TraktIds() { Tmdb = id.ToString() } } });
            WatchListChanged?.Invoke(null, new WatchListArgs(WatchList));
            WatchListResults result = await ChangeWatchList(false, new TraktMovieShow() { Type = MediaType.Movie, Ids = new TraktIds() { Tmdb = id.ToString() } });
            if (result.Added.Movies > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Adds TvShow to WatchList
        /// </summary>
        /// <param name="id">Tmdb TvShow id</param>
        /// <returns>is added</returns>
        internal async static Task<bool> AddTvShowToWatchList(int id)
        {
            WatchList.Add(new UserMovieShow() { ListedAt = System.DateTime.Now, MovieShow = new TraktMovieShow() { Type = MediaType.Show, Ids = new TraktIds() { Tmdb = id.ToString() } } });
            WatchListChanged?.Invoke(null, new WatchListArgs(WatchList));
            WatchListResults result = await ChangeWatchList(false, new TraktMovieShow() { Type = MediaType.Show, Ids = new TraktIds() { Tmdb = id.ToString() } });
            if (result.Added.Shows > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Removes Movie from WatchList
        /// </summary>
        /// <param name="id">Tmdb Movie id</param>
        /// <returns>is removed</returns>
        internal async static Task<bool> RemoveMovieFromWatchList(int id)
        {
            WatchList.RemoveAll(x => x.TmdbId == id);
            WatchListChanged?.Invoke(null, new WatchListArgs(WatchList));
            WatchListResults result = await ChangeWatchList(true, new TraktMovieShow() { Type = MediaType.Movie, Ids = new TraktIds() { Tmdb = id.ToString() } });
            if (result.Deleted.Movies > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Removes TvShow from WatchList
        /// </summary>
        /// <param name="id">Tmdb TvShow id</param>
        /// <returns>Is removed</returns>
        internal async static Task<bool> RemoveTvShowFromWatchList(int id)
        {
            WatchList.RemoveAll(x => x.TmdbId == id);
            WatchListChanged?.Invoke(null, new WatchListArgs(WatchList));
            WatchListResults result = await ChangeWatchList(true, new TraktMovieShow() { Type = MediaType.Show, Ids = new TraktIds() { Tmdb = id.ToString() } });
            if (result.Deleted.Shows > 0)
                return true;
            else
                return false;
        }

        private async static Task<WatchListResults> ChangeWatchList(bool remove, TraktMovieShow movieShow)
        {
            Request req = null;
            if (!remove)
                req = Client.Client.Instance.Create(Request.Service.Trakt, "sync/watchlist");
            else 
                req = Client.Client.Instance.Create(Request.Service.Trakt, "sync/watchlist/remove");
            req.AddTraktLoginRequired();
            if (movieShow.Type == MediaType.Movie )
                req.SetBody(new { movies = new List<TraktMovieShow>() { movieShow } });
            else
                req.SetBody(new { shows = new List<TraktMovieShow>() { movieShow } });
            Response<WatchListResults> response = await req.ExecutePost<WatchListResults>().ConfigureAwait(false);
            return response;
        }

        internal async static Task<bool> IsWatched(ICalendar calendarElement)
        {
            Request req = Client.Client.Instance.Create(Request.Service.Trakt, "sync/history");
            req.AddTraktLoginRequired();
            if (calendarElement.Type == MediaType.Movie)
                req.AddTraktParameter("movies");
            else
                req.AddTraktParameter("episodes");
            req.AddTraktParameter(calendarElement.TraktId);

            JArray response = await req.ExecuteGet<JArray>().ConfigureAwait(false);
            return response.First != null;
        }
    }
    public class WatchListArgs : EventArgs
    {
        public List<UserMovieShow> WatchList { get; set; }

        public WatchListArgs(List<UserMovieShow> watchList)
        {
            WatchList = watchList;
        }
    }
}
