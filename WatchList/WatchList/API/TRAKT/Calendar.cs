﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WatchList.Models.Trakt.Calendar;
using WatchList.Helpers;
using WatchList.Extensions;
using WatchList.API.Client;
using WatchList.Models.Movies;
using WatchList.Models.Trakt.WatchList;
using System.Linq;
using System.Threading;

namespace WatchList.API
{
    internal class Calendar
    {
        private static Calendar ThisObject = null;
        internal static Calendar Instance { get { if (ThisObject == null) ThisObject = new Calendar(); return ThisObject; } }

        internal Calendar()
        {
            API.Account.WatchListChanged += Account_WatchListChanged;
        }

        private async void Account_WatchListChanged(object sender, WatchListArgs e)
        {
            await UpdateCalendar(e.WatchList);
        }

        private enum CalendarType
        {
            [EnumValue("shows")]
            Shows,
            [EnumValue("movies")]
            Movies
        }

        public static List<IMovieTvShow> CalendarWatchList { get; set; }
        internal static bool CalendarUpdated { get; set; }
        private static bool CalendarIsUpdating { get; set; }
        internal async Task UpdateCalendar(List<UserMovieShow> WatchList)
        {
            if (CalendarIsUpdating)
                return;
            if (!Client.Client.IsLoggedIn)
                return;
            SpinWait.SpinUntil(() => Account.WatchListLoaded);
            CalendarIsUpdating = true;
            if (CalendarWatchList == null)
                CalendarWatchList = new List<IMovieTvShow>();
            CalendarUpdated = false;
            CalendarWatchList.RemoveAll(x => !Account.WatchListID.Exists(k => k == x.Id));
            if (CalendarWatchList.Count != WatchList.Count)
                foreach (var item in WatchList)
                {
                    if (item.MovieShow.Type == Models.Trakt.General.MediaType.Movie)
                    {
                        if (!CalendarWatchList.Exists(x => x.Id == item.TmdbId))
                            CalendarWatchList.Add(await Movies.Instance.GetMovieAsync(item.TmdbId));
                    }
                    else
                    {
                        if (!CalendarWatchList.Exists(x => x.Id == item.TmdbId))
                        {
                            Models.TvShows.TvShow tvShow = await TvShows.Instance.GetTvShowAsync(item.TmdbId);
                            tvShow.Ids = item.MovieShow.Ids;
                            tvShow.NextEpisodeAirDate = await API.TvShows.GetTvShowsNextAirTime(tvShow.Ids.Slug);
                            CalendarWatchList.Add(tvShow);
                        }
                    }
                }
            CalendarUpdated = true;
            CalendarIsUpdating = false;
        }
        /// <summary>
        /// Returns all shows airing during the time period specified. Log in required
        /// </summary>
        /// <param name="startDate">Start the calendar on this date</param>
        /// <param name="days">Number of days to display</param>
        /// <returns></returns>
        internal static async Task<List<TvShow>> GetMyShows(DateTime? startDate = null, int days = 0)
        {
            return await GetCalendar<List<TvShow>>(CalendarType.Shows, startDate, days);
        }
        /// <summary>
        /// Returns all movies with a release date during the time period specified. Log in required
        /// </summary>
        /// <param name="startDate">Start the calendar on this date</param>
        /// <param name="days">Number of days to display</param>
        /// <returns></returns>
        internal static async Task<List<Models.Trakt.Calendar.Movies>> GetMyMovies(DateTime? startDate = null, int days = 0)
        {
            return await GetCalendar<List<Models.Trakt.Calendar.Movies>>(CalendarType.Movies, startDate, days);
        }

        private static async Task<T> GetCalendar<T>(CalendarType type, DateTime? startDate = null, int days = 0)
        {
            Request req = Client.Client.Instance.Create(Request.Service.Trakt, "calendars/my/" + type.GetDescription());
            req.AddTraktLoginRequired();
            if (startDate != null)
                req.AddTraktParameter(string.Format("{0}-{1}-{2}", startDate.Value.Year, startDate.Value.Month, startDate.Value.Day));
            if (days > 0)
                req.AddTraktParameter(days.ToString());
            Response<T> response = await req.ExecuteGet<T>().ConfigureAwait(false);
            return response;
        }
    }
}
