﻿using System.Windows.Media.Imaging;
using WatchList.Services;
using WatchList.Models.Movies;
using WatchList.Models.TvShows;
using WatchList.Helpers;
using System.Threading.Tasks;
using WatchList.Models.Search;


namespace WatchList.API
{
    public static class Images
    {
        public static string LoadPoster(string path, string size = "500")
        {
            string CachePath = Cache.LoadPoster(path);
                if (CachePath == null)
                {
                    string fullUrl = GetPosterUrl(size, path);
                    return Cache.SavePoster(fullUrl, path);
                }
                else return CachePath;
        }

        internal static string GetPosterUrl(string size, string posterPath)
        {
            return string.Format("{0}w{1}{2}", API.Constants.Tmdb_Image_Url, size, posterPath);
        }
    }
}
