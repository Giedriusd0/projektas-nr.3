﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WatchList.Extensions;
using WatchList.Helpers;
using WatchList.Models.General;
using WatchList.Models.Search;


namespace WatchList.API
{
    public class Discover
    {
        private static Discover ThisObject = null;
        public static Discover Instance { get { if (ThisObject == null) ThisObject = new Discover(); return ThisObject; } }



        public async Task<SearchContainer<SearchMovie>> GetDiscoveryMovieList(SortBy? sortBy = null, List<string> genreNames = null, int? year = null, int page = 1)
        {
            return await DiscoverMethod<SearchContainer<SearchMovie>>(DiscoverType.Movie, sortBy, genreNames, year, page);
        }

        public async Task<SearchContainer<SearchTv>> GetDiscoveryTvShowList(SortBy? sortBy = null, List<string> genreNames = null, int? year = null,  int page = 1)
        {
            return await DiscoverMethod<SearchContainer<SearchTv>>(DiscoverType.Tv, sortBy, genreNames, year, page);
        }


        private async Task<T> DiscoverMethod<T>(DiscoverType method, SortBy? sortBy, List<string> genreNames, int? year, int page)
        {
            API.Client.Request req = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"discover/{method}");
            req.AddTmdbUrlSegment("method", method.GetDescription());

            if (sortBy != null) {
                SortBy sort = sortBy.Value;
                req.AddTmdbParameter("sort_by", sort.GetDescription());
            }
            if (genreNames != null)
                req.AddTmdbParameterWithComma("with_genres", genreNames);
            if (year != null)
            {
                if(method == DiscoverType.Movie)
                    req.AddTmdbParameter("primary_release_year", year.ToString());
                else
                    req.AddTmdbParameter("first_air_date_year", year.ToString());
            }
            if (page >= 1)
                req.AddTmdbParameter("page", page.ToString());

            API.Client.Response<T> response = await req.ExecuteGet<T>().ConfigureAwait(false);
            var items = await response.GetDataObject().ConfigureAwait(false);
            if (Client.Client.IsLoggedIn)
            {
                SpinWait.SpinUntil(() => Account.WatchListLoaded);
                if (items is SearchContainer<SearchTv>)
                    foreach (SearchTv item in (items as SearchContainer<SearchTv>).Results)
                        item.IsInWatchList = API.Account.IsInWatchList(item.Id);
                else
                    foreach (SearchMovie item in (items as SearchContainer<SearchMovie>).Results)
                        item.IsInWatchList = API.Account.IsInWatchList(item.Id);
            }
            return items;
        }

        [Flags]
        private enum DiscoverType
        {
            [EnumValue("movie")]
            Movie,
            [EnumValue("tv")]
            Tv
        }

        [Flags]
        public enum SortBy
        {
            [EnumValue("popularity.asc")]
            PopularityAscending,
            [EnumValue("popularity.desc")]
            PopularityDescending,
            [EnumValue("release_date.asc")]
            ReleaseDateAscending,
            [EnumValue("release_date.desc")]
            ReleaseDateDescending,
            [EnumValue("revenue.asc")]
            RevenueAscending,
            [EnumValue("revenue.desc")]
            RevenueDescending,
            //[EnumValue("primary_release_date.asc")]
            //PrimaryReleaseDateAscending,
            //[EnumValue("primary_release_date.desc")]
            //PrimaryReleaseDateDescending,
            [EnumValue("original_title.asc")]
            OriginalTitleAscending,
            [EnumValue("original_title.desc")]
            OriginalTitleDescending,
            [EnumValue("vote_average.asc")]
            VoteAverageAscending,
            [EnumValue("vote_average.desc")]
            VoteAverageDescending,
            //[EnumValue("vote_count.desc")]
            //VoteCountAscending,
            //[EnumValue("vote_count.desc")]
            //VoteCountDescending
        }
    }
}
