﻿using Newtonsoft.Json.Linq;
using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WatchList.API.Client;
using WatchList.Extensions;
using WatchList.Models.General;
using WatchList.Models.Search;
using WatchList.Models.TvShows;


namespace WatchList.API
{
    public class TvShows
    {
        private static TvShows ThisObject = null;
        internal static TvShows Instance { get { if (ThisObject == null) ThisObject = new TvShows(); return ThisObject; } }

        public async Task<TvShow> GetLatestTvShowAsync()
        {
            Request req = Client.Client.Instance.Create(Request.Service.Tmdb,"tv/latest");

            Response<TvShow> resp = await req.ExecuteGet<TvShow>().ConfigureAwait(false);

            return resp;
        }

        public async Task<ResultContainer<AlternativeTitle>> GetTvShowAlternativeTitlesAsync(int id)
        {
            return await GetTvShowMethod<ResultContainer<AlternativeTitle>>(id, TvShowMethods.AlternativeTitles).ConfigureAwait(false);
        }

        internal async static Task<DateTime?> GetTvShowsNextAirTime(string slug)
        {
            Request req = Client.Client.Instance.Create(Request.Service.Trakt, "shows");
            req.AddTraktParameter(slug);
            req.AddTraktParameter("next_episode?extended=full");
            JObject json = await req.ExecuteGet<JObject>().ConfigureAwait(false);
            if (json == null)
                return null;
            else
                return json["first_aired"].ToObject<DateTime>();
        }

        public async Task<TvShow> GetTvShowAsync(int id, TvShowMethods extraMethods = TvShowMethods.Undefined, string language = null)
        {
            Request req = Client.Client.Instance.Create(Request.Service.Tmdb,"tv/{id}");
            req.AddTmdbUrlSegment("id", id.ToString(CultureInfo.InvariantCulture));

            language = language ?? Properties.Settings.Default.Language;
            if (!string.IsNullOrWhiteSpace(language))
                req.AddTmdbParameter("language", language);

            string appends = string.Join(",",
                                         Enum.GetValues(typeof(TvShowMethods))
                                             .OfType<TvShowMethods>()
                                             .Except(new[] { TvShowMethods.Undefined })
                                             .Where(s => extraMethods.HasFlag(s))
                                             .Select(s => s.GetDescription()));

            if (appends != string.Empty)
                req.AddTmdbParameter("append_to_response", appends);

            Response<TvShow> response = await req.ExecuteGet<TvShow>().ConfigureAwait(false);

            TvShow item = await response.GetDataObject().ConfigureAwait(false);

            // No data to patch up so return
            if (item == null)
                return null;

            // Patch up data, so that the end user won't notice that we share objects between request-types.
            if (item.Translations != null)
                item.Translations.Id = id;

            if (string.IsNullOrWhiteSpace(item.Overview))
            {
                req.RemoveTmdbParameter("language", language);
                req.AddTmdbParameter("language", Properties.Settings.Default.FallbackLanguage);
                response = await req.ExecuteGet<TvShow>().ConfigureAwait(false);

               TvShow fallbackTvShow = await response.GetDataObject().ConfigureAwait(false);
                item.Overview = fallbackTvShow.Overview;
                item.IsUsingFallbackLanguage = true;
            }

            return item;
        }

        public async Task<ResultContainer<ContentRating>> GetTvShowContentRatingsAsync(int id)
        {
            return await GetTvShowMethod<ResultContainer<ContentRating>>(id, TvShowMethods.ContentRatings).ConfigureAwait(false);
        }

        public async Task<Credits> GetTvShowCreditsAsync(int id)
        {
            return await GetTvShowMethod<Credits>(id, TvShowMethods.Credits, dateFormat: "yyyy-MM-dd", language: Properties.Settings.Default.Language).ConfigureAwait(false);
        }

        public async Task<ImagesWithId> GetTvShowImagesAsync(int id)
        {
            return await GetTvShowMethod<ImagesWithId>(id, TvShowMethods.Images, language: Properties.Settings.Default.Language).ConfigureAwait(false);
        }

        public async Task<ResultContainer<Keyword>> GetTvShowKeywordsAsync(int id)
        {
            return await GetTvShowMethod<ResultContainer<Keyword>>(id, TvShowMethods.Keywords).ConfigureAwait(false);
        }

        private async Task<SearchContainer<SearchTv>> GetTvShowListAsync(int page, string tvShowListType)
        {
            Request req = Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"tv/" + tvShowListType);

            req.AddTmdbParameter("language", Properties.Settings.Default.Language);
            if (page >= 1)
                req.AddTmdbParameter("page", page.ToString());

            Response<SearchContainer<SearchTv>> response = await req.ExecuteGet<SearchContainer<SearchTv>>().ConfigureAwait(false);

            return response;
        }

        public async Task<SearchContainer<SearchTv>> GetTvShowListAsync(TvShowListType list, int page = 0, string timezone = null)
        {
            Request req = Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"tv/{method}");
            req.AddTmdbUrlSegment("method", list.GetDescription());

            if (page > 0)
                req.AddTmdbParameter("page", page.ToString());

            if (!string.IsNullOrEmpty(timezone))
                req.AddTmdbParameter("timezone", timezone);
                req.AddTmdbParameter("language", Properties.Settings.Default.Language);

            Response<SearchContainer<SearchTv>> resp = await req.ExecuteGet<SearchContainer<SearchTv>>().ConfigureAwait(false);

            return resp;
        }

        private async Task<T> GetTvShowMethod<T>(int id, TvShowMethods tvShowMethod, string dateFormat = null, string language = null, int page = 0)
        {
            Request req = Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"tv/{id}/{method}");
            req.AddTmdbUrlSegment("id", id.ToString(CultureInfo.InvariantCulture));
            req.AddTmdbUrlSegment("method", tvShowMethod.GetDescription());

            if (page > 0)
                req.AddTmdbParameter("page", page.ToString());

            language = language ?? Properties.Settings.Default.Language;
            if (!string.IsNullOrWhiteSpace(language))
                req.AddTmdbParameter("language", language);

            Response<T> resp = await req.ExecuteGet<T>().ConfigureAwait(false);

            return resp;
        }

        public async Task<SearchContainer<SearchTv>> GetTvShowPopularAsync(int page = 0)
        {
            return await GetTvShowListAsync(page, "popular").ConfigureAwait(false);
        }

        public async Task<SearchContainer<SearchTv>> GetTvShowSimilarAsync(int id, int page = 0)
        {
            return await GetTvShowMethod<SearchContainer<SearchTv>>(id, TvShowMethods.Similar, language: Properties.Settings.Default.Language, page: page).ConfigureAwait(false);
        }

        public async Task<SearchContainer<SearchTv>> GetTvShowRecommendationsAsync(int id, int page = 0)
        {
            return await GetTvShowMethod<SearchContainer<SearchTv>>(id, TvShowMethods.Recommendations, language: Properties.Settings.Default.Language, page: page).ConfigureAwait(false);
        }

        public async Task<SearchContainer<SearchTv>> GetTvShowTopRatedAsync(int page = 0)
        {
            return await GetTvShowListAsync(page, "top_rated").ConfigureAwait(false);
        }

        public async Task<TranslationsContainerTv> GetTvShowTranslationsAsync(int id)
        {
            return await GetTvShowMethod<TranslationsContainerTv>(id, TvShowMethods.Translations).ConfigureAwait(false);
        }

        public async Task<ResultContainer<Video>> GetTvShowVideosAsync(int id)
        {
            return await GetTvShowMethod<ResultContainer<Video>>(id, TvShowMethods.Videos).ConfigureAwait(false);
        }
    }
}
