﻿using System.Threading.Tasks;
using WatchList.Models.Search;
using WatchList.Models.General;


namespace WatchList.API
{
    public class Search
    {
        private static Search ThisObject = null;
        internal static Search Instance { get { if (ThisObject == null) ThisObject = new Search(); return ThisObject; } }

        public async Task<SearchContainer<SearchCollection>> SearchCollectionAsync(string query, int page = 0)
        {
            return await SearchMethod<SearchContainer<SearchCollection>>("collection", query, page, Properties.Settings.Default.Language).ConfigureAwait(false);
        }


        private async Task<T> SearchMethod<T>(string method, string query, int page, string language = null, bool? includeAdult = null, int year = 0, string dateFormat = null)
        {
            API.Client.Request req = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"search/{method}");
            req.AddTmdbUrlSegment("method", method);
            req.AddTmdbParameter("query", query);

            language = language ?? Properties.Settings.Default.Language;
            if (!string.IsNullOrWhiteSpace(language))
                req.AddTmdbParameter("language", language);

            if (page >= 1)
                req.AddTmdbParameter("page", page.ToString());
            if (year >= 1)
                req.AddTmdbParameter("year", year.ToString());
            if (includeAdult.HasValue)
                req.AddTmdbParameter("include_adult", includeAdult.Value ? "true" : "false");
            
            // TODO: Dateformat?
            //if (dateFormat != null)
            //    req.DateFormat = dateFormat;

            API.Client.Response<T> resp = await req.ExecuteGet<T>().ConfigureAwait(false);

            return resp;
        }

        public async Task<SearchContainer<SearchMovie>> SearchMovieAsync(string query, int page = 0, bool includeAdult = false, int year = 0)
        {
            return await SearchMethod<SearchContainer<SearchMovie>>("movie", query, page, Properties.Settings.Default.Language, includeAdult, year, "yyyy-MM-dd").ConfigureAwait(false);
        }

        public async Task<SearchContainer<SearchBase>> SearchMultiAsync(string query, int page = 0, bool includeAdult = false, int year = 0)
        {
            var multi = await SearchMethod<SearchContainer<SearchBase>>("multi", query, page, Properties.Settings.Default.Language, includeAdult, year, "yyyy-MM-dd").ConfigureAwait(false);
            multi.QueryText = query;
            multi.Results.RemoveAll(x => x == null);
            return multi;
        }

        public async Task<SearchContainer<SearchPerson>> SearchPersonAsync(string query, int page = 0, bool includeAdult = false)
        {
            return await SearchMethod<SearchContainer<SearchPerson>>("person", query, page, includeAdult: includeAdult).ConfigureAwait(false);
        }

        public async Task<SearchContainer<SearchTv>> SearchTvShowAsync(string query, int page = 0)
        {
            return await SearchMethod<SearchContainer<SearchTv>>("tv", query, page).ConfigureAwait(false);
        }
    }
}
