﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using WatchList.Models.General;
using System;

namespace WatchList.API.Configurations
{
    public static class Genre
    {
        public delegate void GenreUpdateHandler(object sender, GenreEventArgs e);
        public static event GenreUpdateHandler GenreUpdated;

        public static GenreContainer MovieGenres = null;
        public static GenreContainer TvGenres = null;

        internal static bool IsLoaded { get; private set; }

        public static async Task Load()
        {
            IsLoaded = false;
            MovieGenres = await GetGenres(GenreTypes.Movie);
            TvGenres = await GetGenres(GenreTypes.Tv);
            IsLoaded = true;
            GenreUpdated?.Invoke(null, new GenreEventArgs(MovieGenres, TvGenres));
        }

        public static List<int> ConvertFromGenreNames(List<string> genreNames)
        {
            System.Threading.SpinWait.SpinUntil(() => IsLoaded);
                List<int> genreIds = new List<int>();
                foreach (var item in genreNames)
                {
                    int? id = MovieGenres?.Genres.FirstOrDefault(x => x.Name == item)?.Id;
                    if (id == null)
                        id = TvGenres?.Genres.FirstOrDefault(x => x.Name == item)?.Id;
                    genreIds.Add((int)id);
                }
                return genreIds;
        }

        public static List<string> ConvertFromGenreIds(List<int> genreIds)
        {
            System.Threading.SpinWait.SpinUntil(() => IsLoaded);
            List<string> genreNames = new List<string>();
            foreach (var item in genreIds)
            {
                string name = MovieGenres?.Genres.FirstOrDefault(x => x.Id == item)?.Name;
                if (string.IsNullOrEmpty(name))
                    name = TvGenres?.Genres?.FirstOrDefault(x => x.Id == item)?.Name;
                genreNames.Add(name);
            }
            return genreNames;
        }

        private static async Task<GenreContainer> GetGenres(GenreTypes type)
        {
            API.Client.Request request = null;
            if(type == GenreTypes.Movie)
                request = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb, "genre/movie/list");
            else
                request = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb, "genre/tv/list");
            request.AddTmdbParameter("language", Properties.Settings.Default.Language);
            API.Client.Response< GenreContainer> response = await request.ExecuteGet<GenreContainer>().ConfigureAwait(false);
            return response;
        }

        private enum GenreTypes
        {
            Tv,
            Movie
        }
    }

    public class GenreEventArgs : EventArgs
    {
        public GenreContainer MovieGenres { get; private set; }
        public GenreContainer TvGenres { get; private set; }

        public GenreEventArgs(GenreContainer movie, GenreContainer tv)
        {
            MovieGenres = movie;
            TvGenres = tv;
        }
    }
}
