﻿using System.Threading.Tasks;
using WatchList.Models.General;


namespace WatchList.API.Configurations
{
    public static class Certifications
    {
        public static Certification MovieCertifications { get; private set; }
        public static Certification TvCertificaitons { get; private set; }


        public static async Task Load()
        {
            MovieCertifications = await GetCertifications(CertificationType.Movie);
            TvCertificaitons = await GetCertifications(CertificationType.TvShow);
        }

        private async static Task<Certification> GetCertifications(CertificationType type)
        {
            API.Client.Request request = null;
            if (type == CertificationType.Movie)
                request = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb, "certification/movie/list");
            else
                request = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb, "certification/tv/list");

            API.Client.Response< Certification> response = await request.ExecuteGet<Certification>().ConfigureAwait(false);
            return response;            
        }

        private enum CertificationType
        {
            Movie,
            TvShow
        }
    }
}
