﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace WatchList.API.Configurations
{
    internal static class ApiConfiguration
    {
        internal enum ImageQuality { Low, Average, High }

        public static Dictionary<string, string> GetCountries()
        {
            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            Dictionary<string, string> countries = new Dictionary<string, string>();
            foreach (CultureInfo culture in cultures)
            {
                RegionInfo region = new RegionInfo(culture.LCID);
                if (!countries.ContainsValue(region.Name))
                    countries.Add(region.EnglishName, region.Name);
            }

            return countries;
        }

        public static string GetCountry(string key = "")
        {
            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo culture in cultures)
            {
                RegionInfo region = new RegionInfo(culture.LCID);
                if (region.NativeName == key)
                    return region.Name;
            }

            return null;
        }

        public static async Task<List<string>> GetTimezones(string key = "")
        {
            API.Client.Request req = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb, "timezones/list");

            API.Client.Response<List<Dictionary<string, List<string>>>> resp = await req.ExecuteGet<List<Dictionary<string, List<string>>>>().ConfigureAwait(false);

            List<Dictionary<string, List<string>>> item = await resp.GetDataObject().ConfigureAwait(false);

            if (item == null)
                return null;

            List<string> timezones = new List<string>();

            foreach (Dictionary<string, List<string>> dictionary in item)
            {
                if (!string.IsNullOrEmpty(key))
                    if (dictionary.First().Key != key)
                        continue;

                //timezones.Add()
                foreach (string timezone in dictionary.First().Value)
                {
                    timezones.Add(timezone);
                }
            }

            return timezones;
        }
    }
}
