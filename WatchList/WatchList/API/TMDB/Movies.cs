﻿using System;
using System.Linq;
using System.Threading.Tasks;
using WatchList.Models.Movies;
using WatchList.Models.General;
using WatchList.Extensions;
using System.Globalization;
using System.Net;
using WatchList.Models.Search;

namespace WatchList.API
{
    public class Movies
    {
        private static Movies ThisObject = null;
        internal static Movies Instance { get { if (ThisObject == null) ThisObject = new Movies(); return ThisObject; } }

        public async Task<AlternativeTitles> GetMovieAlternativeTitlesAsync(int movieId)
        {
            return await GetMovieMethod<AlternativeTitles>(movieId, MovieMethods.AlternativeTitles, language: Properties.Settings.Default.Country).ConfigureAwait(false);
        }

        public async Task<Movie> GetMovieAsync(int movieId, MovieMethods extraMethods = MovieMethods.Undefined)
        {
            return await GetMovieAsync(movieId.ToString(CultureInfo.InvariantCulture), Properties.Settings.Default.Language, extraMethods).ConfigureAwait(false);
        }

        public async Task<Movie> GetMovieAsync(string movieID, string language, MovieMethods extraMethods = MovieMethods.Undefined)
        {
            API.Client.Request req = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"movie/{movieId}");
            req.AddTmdbUrlSegment("movieId", movieID);
            //if (extraMethods.HasFlag(MovieMethods.AccountStates))
            //    TmdbClient.Instance.AddSessionId(req, SessionType.UserSession);

            if (language != null)
                req.AddTmdbParameter("language", language);

            string appends = string.Join(",",
                                         Enum.GetValues(typeof(MovieMethods))
                                             .OfType<MovieMethods>()
                                             .Except(new[] { MovieMethods.Undefined })
                                             .Where(s => extraMethods.HasFlag(s))
                                             .Select(s => s.GetDescription()));

            if (appends != string.Empty)
                req.AddTmdbParameter("append_to_response", appends);

            API.Client.Response<Movie> response = await req.ExecuteGet<Movie>().ConfigureAwait(false);

            // No data to patch up so return
            if (response == null) return null;

            Movie item = await response.GetDataObject().ConfigureAwait(false);

            // Patch up data, so that the end user won't notice that we share objects between req-types.
            if (item.Videos != null)
                item.Videos.Id = item.Id;

            if (item.AlternativeTitles != null)
                item.AlternativeTitles.Id = item.Id;

            if (item.Credits != null)
                item.Credits.Id = item.Id;

            if (item.Releases != null)
                item.Releases.Id = item.Id;

            if (item.Keywords != null)
                item.Keywords.Id = item.Id;

            if (item.Translations != null)
                item.Translations.Id = item.Id;

            if(string.IsNullOrWhiteSpace(item.Overview))
            {
                req.RemoveTmdbParameter("language", language);
                req.AddTmdbParameter("language", Properties.Settings.Default.FallbackLanguage);
                response = await req.ExecuteGet<Movie>().ConfigureAwait(false);

                Movie fallbackMovie = await response.GetDataObject().ConfigureAwait(false);
                item.Overview = fallbackMovie.Overview;
                item.Runtime = fallbackMovie.Runtime;
                item.IsUsingFallbackLanguage = true;
            }

            return item;
        }

        public async Task<Credits> GetMovieCreditsAsync(int movieId)
        {
            return await GetMovieMethod<Credits>(movieId, MovieMethods.Credits).ConfigureAwait(false);
        }

        public async Task<ImagesWithId> GetMovieImagesAsync(int movieId)
        {
            return await GetMovieMethod<ImagesWithId>(movieId, MovieMethods.Images, language: Properties.Settings.Default.Language).ConfigureAwait(false);
        }

        public async Task<KeywordsContainer> GetMovieKeywordsAsync(int movieId)
        {
            return await GetMovieMethod<KeywordsContainer>(movieId, MovieMethods.Keywords).ConfigureAwait(false);
        }

        public async Task<Movie> GetMovieLatestAsync()
        {
            API.Client.Request req = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"movie/latest");
            API.Client.Response<Movie> resp = await req.ExecuteGet<Movie>().ConfigureAwait(false);

            Movie item = await resp.GetDataObject().ConfigureAwait(false);

            // Overview is the only field that is HTML encoded from the source.
            if (item != null)
                item.Overview = WebUtility.HtmlDecode(item.Overview);

            return item;
        }
        
        public async Task<SearchContainerWithId<ListResult>> GetMovieListsAsync(int movieId, int page = 0)
        {
            return await GetMovieMethod<SearchContainerWithId<ListResult>>(movieId, MovieMethods.Lists, page: page, language: Properties.Settings.Default.Language).ConfigureAwait(false);
        }

        public async Task<SearchContainer<SearchMovie>> GetMovieRecommendationsAsync(int id, string language, int page = 0)
        {
            return await GetMovieMethod<SearchContainer<SearchMovie>>(id, MovieMethods.Recommendations, language: Properties.Settings.Default.Language, page: page).ConfigureAwait(false);
        }

        private async Task<T> GetMovieMethod<T>(int movieId, MovieMethods movieMethod, string dateFormat = null,
            string country = null,
            string language = null, int page = 0, DateTime? startDate = null, DateTime? endDate = null)
        {
            API.Client.Request req = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"movie/{movieId}/{method}");
            req.AddTmdbUrlSegment("movieId", movieId.ToString(CultureInfo.InvariantCulture));
            req.AddTmdbUrlSegment("method", movieMethod.GetDescription());

            if (country != null)
                req.AddTmdbParameter("country", country);
            language = language ?? Properties.Settings.Default.Language;
            if (!string.IsNullOrWhiteSpace(language))
                req.AddTmdbParameter("language", language);

            if (page >= 1)
                req.AddTmdbParameter("page", page.ToString());
            if (startDate.HasValue)
                req.AddTmdbParameter("start_date", startDate.Value.ToString("yyyy-MM-dd"));
            if (endDate != null)
                req.AddTmdbParameter("end_date", endDate.Value.ToString("yyyy-MM-dd"));

            API.Client.Response<T> response = await req.ExecuteGet<T>().ConfigureAwait(false);

            return response;
        }

        public async Task<SearchContainerWithDates<SearchMovie>> GetMovieNowPlayingListAsync(string language = null, int page = 0, string region = null)
        {
            API.Client.Request req = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"movie/now_playing");

            if (page >= 1)
                req.AddTmdbParameter("page", page.ToString());
            if (language != null)
                req.AddTmdbParameter("language", language);
            if (region != null)
                req.AddTmdbParameter("region", region);

            API.Client.Response<SearchContainerWithDates<SearchMovie>> resp = await req.ExecuteGet<SearchContainerWithDates<SearchMovie>>().ConfigureAwait(false);

            return resp;
        }

        public async Task<SearchContainer<SearchMovie>> GetMoviePopularListAsync(string language = null, int page = 0, string region = null)
        {
            API.Client.Request req = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"movie/popular");

            if (page >= 1)
                req.AddTmdbParameter("page", page.ToString());
            if (language != null)
                req.AddTmdbParameter("language", language);
            if (region != null)
                req.AddTmdbParameter("region", region);

            API.Client.Response<SearchContainer<SearchMovie>> resp = await req.ExecuteGet<SearchContainer<SearchMovie>>().ConfigureAwait(false);

            return resp;
        }

        public async Task<ResultContainer<ReleaseDatesContainer>> GetMovieReleaseDatesAsync(int movieId)
        {
            return await GetMovieMethod<ResultContainer<ReleaseDatesContainer>>(movieId, MovieMethods.ReleaseDates).ConfigureAwait(false);
        }

        public async Task<Releases> GetMovieReleasesAsync(int movieId)
        {
            return await GetMovieMethod<Releases>(movieId, MovieMethods.Releases, dateFormat: "yyyy-MM-dd").ConfigureAwait(false);
        }

        public async Task<SearchContainer<SearchMovie>> GetMovieSimilarAsync(int movieId, string language, int page = 0)
        {
            return await GetMovieMethod<SearchContainer<SearchMovie>>(movieId, MovieMethods.Similar, page: page, language: Properties.Settings.Default.Language, dateFormat: "yyyy-MM-dd").ConfigureAwait(false);
        }

        public async Task<SearchContainer<SearchMovie>> GetMovieTopRatedListAsync(int page = 0, string region = null)
        {
            API.Client.Request req = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"movie/top_rated");

            if (page >= 1)
                req.AddTmdbParameter("page", page.ToString());
            if (region != null)
                req.AddTmdbParameter("region", region);
            req.AddTmdbParameter("language", Properties.Settings.Default.Language);

            API.Client.Response<SearchContainer<SearchMovie>> resp = await req.ExecuteGet<SearchContainer<SearchMovie>>().ConfigureAwait(false);

            return resp;
        }

        public async Task<TranslationsContainer> GetMovieTranslationsAsync(int movieId)
        {
            return await GetMovieMethod<TranslationsContainer>(movieId, MovieMethods.Translations).ConfigureAwait(false);
        }

        public async Task<SearchContainerWithDates<SearchMovie>> GetMovieUpcomingListAsync(int page = 0, string region = null)
        {
            API.Client.Request req = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"movie/upcoming");

            if (page >= 1)
                req.AddTmdbParameter("page", page.ToString());
                req.AddTmdbParameter("language", Properties.Settings.Default.Language);
            if (region != null)
                req.AddTmdbParameter("region", region);

            API.Client.Response<SearchContainerWithDates<SearchMovie>> resp = await req.ExecuteGet<SearchContainerWithDates<SearchMovie>>().ConfigureAwait(false);

            return resp;
        }

        public async Task<ResultContainer<Video>> GetMovieVideosAsync(int movieId)
        {
            ResultContainer<Video> video = await GetMovieMethod<ResultContainer<Video>>(movieId, MovieMethods.Videos).ConfigureAwait(false);
            if (video.Results.Count == 0)
                video = await GetMovieMethod<ResultContainer<Video>>(movieId, MovieMethods.Videos, language: Properties.Settings.Default.FallbackLanguage).ConfigureAwait(false);
            return video;
        }

        //public async Task<bool> MovieRemoveRatingAsync(int movieId)
        //{
        //    API.Client.Request req = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"movie/{movieId}/rating");
        //    req.AddTmdbUrlSegment("movieId", movieId.ToString(CultureInfo.InvariantCulture));
        //    TmdbClient.Instance.AddSessionId(req);

        //    API.Client.Response<PostReply> response = await req.ExecuteDelete<PostReply>().ConfigureAwait(false);

        //    // status code 13 = "The item/record was deleted successfully."
        //    PostReply item = await response.GetDataObject().ConfigureAwait(false);

        //    // TODO: Previous code checked for item=null
        //    return item != null && item.StatusCode == 13;
        //}

        /// <summary>
        /// Change the rating of a specified movie.
        /// </summary>
        /// <param name="movieId">The id of the movie to rate</param>
        /// <param name="rating">The rating you wish to assign to the specified movie. Value needs to be between 0.5 and 10 and must use increments of 0.5. Ex. using 7.1 will not work and return false.</param>
        /// <returns>True if the the movie's rating was successfully updated, false if not</returns>
        /// <remarks>Requires a valid guest or user session</remarks>
        /// <exception cref="GuestSessionRequiredException">Thrown when the current client object doens't have a guest or user session assigned.</exception>
        //public async Task<bool> MovieSetRatingAsync(int movieId, double rating)
        //{
        //    API.Client.Request req = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"movie/{movieId}/rating");
        //    req.AddUrlSegment("movieId", movieId.ToString(CultureInfo.InvariantCulture));
        //    TmdbClient.Instance.AddSessionId(req);

        //    req.SetBody(new { value = rating });

        //    API.Client.Response<PostReply> response = await req.ExecutePost<PostReply>().ConfigureAwait(false);

        //    // status code 1 = "Success"
        //    // status code 12 = "The item/record was updated successfully" - Used when an item was previously rated by the user
        //    PostReply item = await response.GetDataObject().ConfigureAwait(false);

        //    // TODO: Previous code checked for item=null
        //    return item.StatusCode == 1 || item.StatusCode == 12;
        //}
    }
}
