﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WatchList.Extensions;
using WatchList.Models.General;
using WatchList.Models.TvShows;


namespace WatchList.API
{
    public static class TvShowSeason
    {

        public static async Task<TvSeason> GetTvSeasonAsync(int tvShowId, int seasonNumber, TvSeasonMethods extraMethods = TvSeasonMethods.Undefined)
        {
            API.Client.Request req = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"tv/{id}/season/{season_number}");
            req.AddTmdbUrlSegment("id", tvShowId.ToString(CultureInfo.InvariantCulture));
            req.AddTmdbUrlSegment("season_number", seasonNumber.ToString(CultureInfo.InvariantCulture));
            req.AddTmdbParameter("language", Properties.Settings.Default.Language);

            string appends = string.Join(",",
                                         Enum.GetValues(typeof(TvSeasonMethods))
                                             .OfType<TvSeasonMethods>()
                                             .Except(new[] { TvSeasonMethods.Undefined })
                                             .Where(s => extraMethods.HasFlag(s))
                                             .Select(s => s.GetDescription()));

            if (appends != string.Empty)
                req.AddTmdbParameter("append_to_response", appends);

            API.Client.Response<TvSeason> response = await req.ExecuteGet<TvSeason>().ConfigureAwait(false);

            TvSeason item = await response.GetDataObject().ConfigureAwait(false);

            // Nothing to patch up
            if (item == null)
                return null;

            if (item.Images != null)
                item.Images.Id = item.Id ?? 0;

            if (item.Credits != null)
                item.Credits.Id = item.Id ?? 0;

            if (item.AccountStates != null)
                item.AccountStates.Id = item.Id ?? 0;

            if (item.Videos != null)
                item.Videos.Id = item.Id ?? 0;

            return item;
        }

        public static async Task<Credits> GetTvSeasonCreditsAsync(int tvShowId, int seasonNumber)
        {
            return await GetTvSeasonMethod<Credits>(tvShowId, seasonNumber, TvSeasonMethods.Credits, dateFormat: "yyyy-MM-dd").ConfigureAwait(false);
        }

        public static async Task<PosterImages> GetTvSeasonImagesAsync(int tvShowId, int seasonNumber)
        {
            return await GetTvSeasonMethod<PosterImages>(tvShowId, seasonNumber, TvSeasonMethods.Images).ConfigureAwait(false);
        }

        private static async Task<T> GetTvSeasonMethod<T>(int tvShowId, int seasonNumber, TvSeasonMethods tvShowMethod, string dateFormat = null)
        {
            API.Client.Request req = API.Client.Client.Instance.Create(API.Client.Request.Service.Tmdb,"tv/{id}/season/{season_number}/{method}");
            req.AddTmdbUrlSegment("id", tvShowId.ToString(CultureInfo.InvariantCulture));
            req.AddTmdbUrlSegment("season_number", seasonNumber.ToString(CultureInfo.InvariantCulture));
            req.AddTmdbUrlSegment("method", tvShowMethod.GetDescription());

            req.AddTmdbParameter("language", Properties.Settings.Default.Language);

            API.Client.Response<T> response = await req.ExecuteGet<T>().ConfigureAwait(false);

            return response;
        }

        public static async Task<ResultContainer<Video>> GetTvSeasonVideosAsync(int tvShowId, int seasonNumber)
        {
            return await GetTvSeasonMethod<ResultContainer<Video>>(tvShowId, seasonNumber, TvSeasonMethods.Videos).ConfigureAwait(false);
        }
    }
}
