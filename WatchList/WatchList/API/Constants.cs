﻿namespace WatchList.API
{
    internal class Constants
    {
        internal const string Trakt_Client_ID = "16d1133e8c38e245c023a68715fb6a5bef7a32d88e6baa2ecc49b0d72e9cba29";
        internal const string Trakt_Client_Secret = "99d15bbecd46a9c9149c9f5e32475f0f7a0322577a205b2c1e62c747977eb965";
        internal const string Tmdb_v3_API_Key = "ef3e6f9c8223a65cf1b468eb0bf4fd14";

        internal const string Trakt_BaseUrl = "https://api.trakt.tv/";
        internal const string Tmdb_BaseUrl = "https://api.themoviedb.org/3/";

        internal const string Trakt_RedirectUrl = "urn:ietf:wg:oauth:2.0:oob";
        internal const string Tmdb_Image_Url = "https://image.tmdb.org/t/p/";
    }
}
