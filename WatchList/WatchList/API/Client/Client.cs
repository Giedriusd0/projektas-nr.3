﻿using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WatchList.Converters;
using WatchList.Models.Trakt.Authentication;

namespace WatchList.API.Client
{
    internal class Client
    {
        internal static Client Instance { get; private set; }

        public delegate void EventHandler(Object sender, UserSessionArgs e);
        public static event EventHandler UserSessionChanged;

        private TraktAuthorization _Session = Services.Cache.RestoreAuth();
        internal TraktAuthorization Session { get { return _Session; } set { _Session = value; SetCurrentUser();  } }
        internal static bool IsLoggedIn => Instance.Session?.AccessToken != null;
        internal static User CurrentUser { get; private set; }

        internal JsonSerializer Serializer { get; }
        internal Encoding Encoding { get; } = new UTF8Encoding(false);

        internal static void LoadConfig()
        {
            Instance = new Client();
            Task.Run(() => { Services.Youtube.Instance.Update(); });
            Task traktWatchList = Task.Run(Account.UpdateWatchList);
            Task tmdbGenres = Task.Run(Configurations.Genre.Load);
            Task.Run(async ()=>
            {
                SpinWait.SpinUntil(() => Account.WatchListLoaded);
                await Calendar.Instance.UpdateCalendar(Account.WatchList);
            });
            if (Instance.Session?.ExpiresInSeconds < 86400)
            {
                Task SessionRefresh = Task.Run(async () => { await Authentication.GetAuthorizationAsync(); });
                System.Threading.SpinWait.SpinUntil(() => SessionRefresh.IsCompleted);
            }
            Instance.SetCurrentUser();
        }

        internal void RemoveSession()
        {
            Session = null;
        }

        public Client()
        {
            Serializer = MakeSerializer();
        }

        private async void SetCurrentUser()
        {
            if (IsLoggedIn)
            {
                Settings account = await Account.GetUserSettings();
                if(account.Account.TimeZone != "")
                    Properties.Settings.Default.Timezone = account.Account.TimeZone;
                if (account.User.Location != "")
                    Properties.Settings.Default.Country = Configurations.ApiConfiguration.GetCountry(account.User.Location);
                Properties.Settings.Default.Save();
                CurrentUser = account.User;
                UserSessionChanged?.Invoke(null, new UserSessionArgs(true, CurrentUser));
            }
            else
            {
                CurrentUser = null;
                UserSessionChanged?.Invoke(null, new UserSessionArgs(false, null));

            }
        }

        private JsonSerializer MakeSerializer()
        {
            JsonSerializer _serializer = JsonSerializer.CreateDefault();
            _serializer.Converters.Add(new SearchBaseConverter());
            _serializer.Converters.Add(new UserMediaBaseConverter());
            _serializer.Converters.Add(new TolerantEnumConverter());
            return _serializer;
        }

        public Request Create(Request.Service service, string endpoint)
        {
            return new Request(service, endpoint);
        }
    }

    public class UserSessionArgs : EventArgs
    {
        public bool LoggedIn { get; set; }
        public User AccountDetails { get; set; }

        public UserSessionArgs(bool loggedIn, User details)
        {
            LoggedIn = loggedIn;
            AccountDetails = details;
        }
    }
}
