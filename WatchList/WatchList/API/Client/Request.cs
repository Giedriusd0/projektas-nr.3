﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace WatchList.API.Client
{
    internal class Request
    {
        internal enum Service { Tmdb, Trakt}
        private readonly string _EndUrl;
        private object _PostBody;

        private bool IsLoginRequired;

        private List<KeyValuePair<string, string>> TmdbQueryString;
        private List<KeyValuePair<string, string>> TmdbUrlSegment;
        private Service CurrentService { get; }
        private List<string> TraktQueryString;

        public Request(Service service, string endUrl)
        {
            CurrentService = service;
            _EndUrl = endUrl;
        }

        public Request AddTmdbParameter(string key, string value)
        {
            if (TmdbQueryString == null)
                TmdbQueryString = new List<KeyValuePair<string, string>>();

            TmdbQueryString.Add(new KeyValuePair<string, string>(key, value));

            return this;
        }

        public Request AddTraktParameter(string value)
        {
            if (TraktQueryString == null)
                TraktQueryString = new List<string>();

            TraktQueryString.Add(value);

            return this;
        }

        public Request RemoveTmdbParameter(string key, string value)
        {
            if (TmdbQueryString != null)
                TmdbQueryString.Remove(new KeyValuePair<string, string>(key, value));

            return this;
        }

        public Request AddTmdbParameterWithComma(string key, List<string> list)
        {
            string value = String.Join(",", API.Configurations.Genre.ConvertFromGenreNames(list));

            if (TmdbQueryString == null)
                TmdbQueryString = new List<KeyValuePair<string, string>>();

            TmdbQueryString.Add(new KeyValuePair<string, string>(key, value));

            return this;
        }

        public Request AddTmdbUrlSegment(string key, string value)
        {
            if (TmdbUrlSegment == null)
                TmdbUrlSegment = new List<KeyValuePair<string, string>>();

            TmdbUrlSegment.Add(new KeyValuePair<string, string>(key, value));

            return this;
        }

        public Request AddTraktLoginRequired()
        {
            IsLoginRequired = true;
            return this;
        }

        private void AppendTmdbQueryString(StringBuilder sb, KeyValuePair<string, string> value)
        {
            if (sb.Length > 0)
                sb.Append("&");

            sb.Append(value.Key);
            sb.Append("=");
            sb.Append(WebUtility.UrlEncode(value.Value));
        }

        private void AppendTraktQueryString(StringBuilder sb, string value)
        {
            sb.Append("/" + value);
        }


        public async Task<Response<T>> ExecuteDelete<T>()
        {
            HttpResponseMessage resp = await SendInternal(HttpMethod.Delete).ConfigureAwait(false);

            return new Response<T>(resp);
        }

        public async Task<Response<T>> ExecuteGet<T>()
        {
            HttpResponseMessage resp = await SendInternal(HttpMethod.Get).ConfigureAwait(false);

            return new Response<T>(resp);
        }

        public async Task<Response<T>> ExecutePost<T>()
        {
            HttpResponseMessage resp = await SendInternal(HttpMethod.Post).ConfigureAwait(false);

            return new Response<T>(resp);
        }

        public async Task ExecutePost()
        {
            HttpResponseMessage resp = await SendInternal(HttpMethod.Post).ConfigureAwait(false);
        }

        private HttpRequestMessage PrepRequest(HttpMethod method)
        {
            StringBuilder queryStringSb = new StringBuilder();
            string endpoint = _EndUrl;
            HttpRequestMessage req = null;
            if (CurrentService == Service.Tmdb)
            {
                if (TmdbQueryString != null)
                {
                    foreach (KeyValuePair<string, string> pair in TmdbQueryString)
                        AppendTmdbQueryString(queryStringSb, pair);
                }
                AppendTmdbQueryString(queryStringSb, new KeyValuePair<string, string>("api_key", Constants.Tmdb_v3_API_Key));

                if (TmdbUrlSegment != null)
                {
                    foreach (KeyValuePair<string, string> pair in TmdbUrlSegment)
                        endpoint = endpoint.Replace("{" + pair.Key + "}", pair.Value);
                }
                // Build
                UriBuilder builder = new UriBuilder(new Uri(new Uri(Constants.Tmdb_BaseUrl), endpoint));
                builder.Query = queryStringSb.ToString();

                req = new HttpRequestMessage(method, builder.Uri);
            }
            else
            {
                if (TraktQueryString != null)
                {
                    foreach (string pair in TraktQueryString)
                        AppendTraktQueryString(queryStringSb, pair);
                }
                Uri uri = new Uri(string.Format("{0}{1}{2}", Constants.Trakt_BaseUrl, endpoint, queryStringSb));
                req = new HttpRequestMessage(method, uri);
            }
            // Body
            if (method == HttpMethod.Post && _PostBody != null)
            {
                MemoryStream ms = new MemoryStream();
                using (StreamWriter sw = new StreamWriter(ms, Client.Instance.Encoding, 4096, true))
                    using (JsonTextWriter tw = new JsonTextWriter(sw))
                        Client.Instance.Serializer.Serialize(tw, _PostBody);

                ms.Seek(0, SeekOrigin.Begin);

                req.Content = new StreamContent(ms);
                req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            }

            return req;
        }

        private async Task<HttpResponseMessage> SendInternal(HttpMethod method)
        {
            // Account for the following settings:
            // - MaxRetryCount                          Max times to retry
            // DEPRECATED RetryWaitTimeInSeconds        Time to wait between retries
            // DEPRECATED ThrowErrorOnExeedingMaxCalls  Throw an exception if we hit a ratelimit

            //int timesToTry = _client.MaxRetryCount + 1;

            //Debug.Assert(timesToTry >= 1);
            HttpRequestMessage req = PrepRequest(method);
            HttpClientHandler handler = new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate };
            HttpClient client = new HttpClient(handler);
            if (CurrentService == Service.Trakt)
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation("trakt-api-version", "2");
                client.DefaultRequestHeaders.TryAddWithoutValidation("trakt-api-key", Constants.Trakt_Client_ID);
                if (IsLoginRequired)
                    client.DefaultRequestHeaders.TryAddWithoutValidation("authorization", "Bearer " + Client.Instance.Session.AccessToken);
            }
            HttpResponseMessage resp = null;
            resp = await client.SendAsync(req).ConfigureAwait(false);
            if (resp.StatusCode == (HttpStatusCode)429)
            {
                // The previous result was a ratelimit, read the Retry-After header and wait the allotted time
                TimeSpan? retryAfter = resp.Headers.RetryAfter?.Delta.Value;

                if (retryAfter.HasValue && retryAfter.Value.TotalSeconds > 0)
                    await Task.Delay(retryAfter.Value).ConfigureAwait(false);
                else
                    // TMDb sometimes gives us 0-second waits, which can lead to rapid succession of requests
                    await Task.Delay(TimeSpan.FromSeconds(1)).ConfigureAwait(false);
            }
            return resp;
        }

        public Request SetBody(object obj)
        {
            _PostBody = obj;

            return this;
        }
    }
}
