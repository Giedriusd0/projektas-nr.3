﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace WatchList.API.Client
{
    internal class Response<T>
    {
        protected readonly HttpResponseMessage HttpResponse;

        public Response(HttpResponseMessage response)
        {
            HttpResponse = response;
        }

        public HttpStatusCode StatusCode => HttpResponse.StatusCode;

        public async Task<Stream> GetContent()
        {
            return await HttpResponse.Content.ReadAsStreamAsync().ConfigureAwait(false);
        }

        public string GetHeader(string name, string @default = null)//Isn't used
        {
            return HttpResponse.Headers.GetValues(name).FirstOrDefault() ?? @default;
        }

        public async Task<T> GetDataObject()
        {
            Stream content = await GetContent().ConfigureAwait(false);

            using (StreamReader sr = new StreamReader(content, Client.Instance.Encoding))
            using (JsonTextReader tr = new JsonTextReader(sr))
                return Client.Instance.Serializer.Deserialize<T>(tr);
        }

        public static implicit operator T(Response<T> response)
        {
            try
            {
                return response.GetDataObject().Result;
            }
            catch (AggregateException ex)
            {
                throw ex.InnerException;
            }
        }
    }
}
