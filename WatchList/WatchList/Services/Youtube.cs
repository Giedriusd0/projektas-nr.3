﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using WatchList.Helpers;
using WatchList.Models.General;

namespace WatchList.Services
{
    public class Youtube
    {
        private static Youtube ThisObject = null;
        internal static Youtube Instance { get { if (ThisObject == null) ThisObject = new Youtube(); return ThisObject; } }
        Process YoutubeProcees = null;
        private bool IsUpdating { get; set; } = false;

        public Youtube()
        {
            SetYoutubeProcess();
        }

        private void SetYoutubeProcess()
        {
            string path = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase))) + "\\Services";
            YoutubeProcees = new Process();
            YoutubeProcees.StartInfo.UseShellExecute = false;
            YoutubeProcees.StartInfo.RedirectStandardOutput = true;
            YoutubeProcees.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            path += "\\youtube-dl.exe";
            path = path.Remove(0, 6);
            var fileInfo = new FileInfo(path);
            YoutubeProcees.StartInfo.FileName = fileInfo.FullName;
            YoutubeProcees.StartInfo.CreateNoWindow = true;
            YoutubeProcees.EnableRaisingEvents = true;
            if (!fileInfo.Exists)
            {
                throw new FileNotFoundException("The requested file was not found: " + fileInfo.FullName);
            }
        }

        public void Update() //Remake Youtube update
        {
            IsUpdating = true;
            YoutubeProcees.StartInfo.Arguments = string.Format(@"--update");
            YoutubeProcees.Start();
            while (!YoutubeProcees.StandardOutput.EndOfStream)
                YoutubeProcees.StandardOutput.ReadLine();
                //    Console.WriteLine(YoutubeProcees.StandardOutput.ReadLine());

            //System.Threading.SpinWait.SpinUntil(() => YoutubeProcees.StandardOutput.EndOfStream);
            IsUpdating = false;
        }
        /// <summary>
        /// Gets available Qualities
        /// </summary>
        /// <param name="id">url ending</param>
        /// <returns></returns>
        public List<Quality> GetAvailableVideoQuality(string id)
        {
            System.Threading.SpinWait.SpinUntil(() => !IsUpdating);
            YoutubeProcees.StartInfo.Arguments = string.Format(@"-F https://www.youtube.com/watch?v={0}", id);
            YoutubeProcees.Start();

            List<Quality> quality = new List<Quality>();
            while (!YoutubeProcees.StandardOutput.EndOfStream)
            {
                string line = YoutubeProcees.StandardOutput.ReadLine();
                line = line.Substring(0, 2);
                switch (line)
                {
                    case "36":
                        quality.Add(Quality.p360);
                        break;
                    case "18":
                        quality.Add(Quality.p480);
                        break;
                    case "22":
                        quality.Add(Quality.HD720);
                        break;
                }
            }
            return quality;
        }
        /// <summary>
        /// Gets video Url, with selected quality
        /// </summary>
        /// <param name="quality"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public YoutubeVideo GetVideoUrl(Quality quality, string id)
        {
            System.Threading.SpinWait.SpinUntil(() => !IsUpdating);
            YoutubeProcees.StartInfo.Arguments = string.Format(@"-e -f {0} -g -- {1}", (int)quality, id);
            YoutubeProcees.Start();
            string title = string.Empty;
            string url = string.Empty;
            while (!YoutubeProcees.StandardOutput.EndOfStream)
            {
                title = YoutubeProcees.StandardOutput.ReadLine();
                url = YoutubeProcees.StandardOutput.ReadLine();
            }
            return new YoutubeVideo(title,url);
        }

        public YoutubeVideo GetVideoUrl(string id)
        {
            System.Threading.SpinWait.SpinUntil(() => !IsUpdating);
            YoutubeProcees.StartInfo.Arguments = string.Format(@"-e -f best[height<=720] -g -- {0}", id);
            YoutubeProcees.Start();
            string title = string.Empty;
            string url = string.Empty;
            while (!YoutubeProcees.StandardOutput.EndOfStream)
            {
                title = YoutubeProcees.StandardOutput.ReadLine();
                url = YoutubeProcees.StandardOutput.ReadLine();
            }
            return new YoutubeVideo(title, url);
        }

        public YoutubeVideo GetVideoUrl(Quality quality, ResultContainer<Video> videos)
        {
            System.Threading.SpinWait.SpinUntil(() => !IsUpdating);
            foreach (var item in videos.Results)
            {
                YoutubeProcees.StartInfo.Arguments = string.Format(@"-e -f {0} -g -- {1}", (int)quality, item.Key);
                YoutubeProcees.Start();
                string title = string.Empty;
                string url = string.Empty;
                while (!YoutubeProcees.StandardOutput.EndOfStream)
                {
                    title = YoutubeProcees.StandardOutput.ReadLine();
                    url = YoutubeProcees.StandardOutput.ReadLine();
                }
                if (string.IsNullOrEmpty(title) && string.IsNullOrEmpty(url))
                    return new YoutubeVideo(title, url);
            }
            return null;
        }

        public enum Quality
        {
            p360 = 36,
            p480 = 18,
            HD720 = 22
        }
    }
}
