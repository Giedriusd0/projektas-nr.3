﻿using ProtoBuf;
using System;
using System.IO;
using System.Net;

namespace WatchList.Services
{
    class Cache
    {
        private static string AuthFile = Helpers.GeneralHelpers.GetFolder(Helpers.GeneralHelpers.FolderTypes.Auth) + "Auth.WatchList";

        public static void SaveAuth(Models.Trakt.Authentication.TraktAuthorization session)
        {
            using(var file = File.Create(AuthFile))
            {
                Serializer.Serialize(file, session);
            };
        }

        public static Models.Trakt.Authentication.TraktAuthorization RestoreAuth()
        {
            if (File.Exists(AuthFile))
                using (var file = File.OpenRead(AuthFile))
                    try {
                        return Serializer.Deserialize<Models.Trakt.Authentication.TraktAuthorization>(file);
                    }
                    catch { RemoveAuth(); return null; }
            else return null;
        }

        public static bool RemoveAuth()
        {
            if (File.Exists(AuthFile))
            {
                File.Delete(AuthFile);
                return true;
            }
            else
                return false;
        }

        public static string LoadPoster(string path)
        {
            path = path.Remove(0, 1);
            if (File.Exists(Helpers.GeneralHelpers.GetFolder(Helpers.GeneralHelpers.FolderTypes.Posters) + path))
                return Helpers.GeneralHelpers.GetFolder(Helpers.GeneralHelpers.FolderTypes.Posters) + path;
            else
                return null;
        }

        public static string SavePoster(string url, string path)
        {
            path = path.Remove(0, 1);
            using (WebClient client = new WebClient())
            {
                client.DownloadFile(new Uri(url), Helpers.GeneralHelpers.GetFolder(Helpers.GeneralHelpers.FolderTypes.Posters) + path);
            }
            return Helpers.GeneralHelpers.GetFolder(Helpers.GeneralHelpers.FolderTypes.Posters) + path;
        }
        
        public static void ClearCache()
        {
             DirectoryInfo di = new DirectoryInfo(Helpers.GeneralHelpers.GetFolder(Helpers.GeneralHelpers.FolderTypes.Cache));
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
        }
    }
}
