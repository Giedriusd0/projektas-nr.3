﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using WatchList.Models.Trakt.General;
using WatchList.Models.Trakt.WatchList;

namespace WatchList.Converters
{
    internal class UserMediaBaseConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(UserMovieShow);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jObject = JObject.Load(reader);

            UserMovieShow result;
            // Determine the type based on the media_type
            MediaType mediaType = jObject["type"].ToObject<MediaType>();
            result = new UserMovieShow();
            result.MovieShow = new TraktMovieShow();

            switch (mediaType)
            {
                case MediaType.Movie:
                    result.MovieShow = jObject["movie"].ToObject<TraktMovieShow>();
                    result.MovieShow.Type = MediaType.Movie;
                    break;
                case MediaType.Show:
                    result.MovieShow = jObject["show"].ToObject<TraktMovieShow>();
                    result.MovieShow.Type = MediaType.Show;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            // Populate the result
            serializer.Populate(jObject.CreateReader(), result);

            return result;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            JToken jToken = JToken.FromObject(value);

            jToken.WriteTo(writer);
        }
    }
}
