﻿using System;
using System.Globalization;
using System.Windows.Data;
using WatchList.Models.Search;
using WatchList.API;

namespace WatchList.Converters
{
    public class PosterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value != null)
            switch ((string)parameter)
            {
                case "Small": return Helpers.GeneralHelpers.GetUnlockedImage(Images.LoadPoster(value.ToString(), "92"), 138);
                case "Medium": return Helpers.GeneralHelpers.GetUnlockedImage(Images.LoadPoster(value.ToString(), "342"), 133);
                case "Large": return Helpers.GeneralHelpers.GetUnlockedImage(Images.LoadPoster(value.ToString(), "500"), 200);
                default: return "../Resources/Images/poster_fallback.jpg";
            }
            return "../Resources/Images/poster_fallback.jpg";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
