﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using static WatchList.API.Discover;

namespace WatchList.Converters
{
    public class SortByConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
                switch ((string)value)
                {
                    case "OriginalTitleAscending": return Properties.Resources.sortByOriginalTitleAscending.ToString();
                    case "OriginalTitleDescending": return Properties.Resources.sortByOriginalTitleDescending;
                    case "PopularityAscending": return Properties.Resources.sortByPopularityAscending;
                    case "PopularityDescending": return Properties.Resources.sortByPopularityDescending;
                    case "PrimaryReleaseDateAscending": return Properties.Resources.sortByPrimaryReleaseDateAscending;
                    case "PrimaryReleaseDateDescending": return Properties.Resources.sortByPrimaryReleaseDateDescending;
                    case "ReleaseDateAscending": return Properties.Resources.sortByReleaseDateAscending;
                    case "ReleaseDateDescending": return Properties.Resources.sortByReleaseDateDescending;
                    case "RevenueAscending": return Properties.Resources.sortByRevenueAscending;
                    case "RevenueDescending": return Properties.Resources.sortByRevenueDescending;
                    case "VoteAverageAscending": return Properties.Resources.sortByVoteAverageAscending;
                    case "VoteAverageDescending": return Properties.Resources.sortByVoteAverageDescening;
                    case "VoteCountAscending": return Properties.Resources.sortByVoteAverageAscending;
                    case "VoteCountDescending": return Properties.Resources.sortByVoteCountDescending;
                    default: return "RESOURCE NOT FOUND";
                }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
