﻿  using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WatchList.API.Configurations;
using WatchList.Views;

namespace WatchList.ViewModels
{
    public class SettingsModel : HomeModel
    {
        #region Privates
        public enum AccountStatus { LoggedIn, LoggedOut, AwaitingCode }
        private AccountStatus _CurrentStatus;
        private string _PinCode;
        private int _SelectedLanguage = 0;
        private int _SelectedFallBackLanguage = 0;
        private int _SelectedCountry = 0;
        private int _SelectedTimezone = 0;
        private int _SelectedTheme = 0;
        private bool _LogInVisibility = true;
        private bool _LoggedInVisibility = false;
        private bool _AwaitingPinCodeVisibility = false;
        //private TMDB.Account AccountInstance = TMDB.Account.Instance;
        //private TMDB.Authentication AuthenticationInstance = TMDB.Authentication.Instance;
        private bool _RunOnBoot = CheckRunOnBoot();
        private List<string> _FallBackLanguage;
        private Dictionary<string, string> _CountryList;
        private List<string> _TimezoneList;
        private List<string> _ThemeList = new List<string>() { "Standard" };
        private string _UserName = string.Empty;
        #endregion

        #region Public
        public bool RunOnBoot { get { return _RunOnBoot; } set { _RunOnBoot = value; RegisterInStartup(value); OnPropertyChanged(); } }
        public bool Notifications { get { return Properties.Settings.Default.Notifications; } set { Properties.Settings.Default.Notifications = value; OnPropertyChanged(); } }
        public int SelectedLanguage { get { return _SelectedLanguage; } set { _SelectedLanguage = value; OnPropertyChanged(); } }
        public int SelectedFallBackLanguage { get { return _SelectedFallBackLanguage; } set { _SelectedFallBackLanguage = value; OnPropertyChanged(); } }
        public int SelectedCountry { get { return _SelectedCountry; } set { _SelectedCountry = value; CountryChanged(); OnPropertyChanged(); } }
        public int SelectedTimezone { get { return _SelectedTimezone; } set { _SelectedTimezone = value; OnPropertyChanged(); } }
        public int SelectedTheme { get { return _SelectedTheme; } set { _SelectedTheme = value; OnPropertyChanged(); } }
        public List<string> CountryList { get { return _CountryList.Keys.ToList(); } }
        public List<string> TimezoneList { get { return _TimezoneList; } set { _TimezoneList = value; OnPropertyChanged(); } }
        public List<string> ThemeList { get { return _ThemeList; } set { _ThemeList = value; OnPropertyChanged(); } }
        public List<string> FallBackLanguage { get { return _FallBackLanguage; } set { _FallBackLanguage = value; OnPropertyChanged(); } }
        public bool LogInVisibility { get { return _LogInVisibility; } set { _LogInVisibility = value; OnPropertyChanged(); } }
        public bool LoggedInVisibility { get { return _LoggedInVisibility; } set { _LoggedInVisibility = value; OnPropertyChanged(); } }
        public bool AwaitingPinCodeVisibility { get { return _AwaitingPinCodeVisibility; } set { _AwaitingPinCodeVisibility = value; OnPropertyChanged(); } }
        public string UserName { get { return _UserName; } set { _UserName = value; OnPropertyChanged(); } }
        public string PinCode { get { return _PinCode; } set { _PinCode = value; OnPropertyChanged(); } }
        public List<string> LanguageList { get; private set; }
        public AccountStatus CurrentStatus { get { return _CurrentStatus; } set { _CurrentStatus = value; LogInVisibilityChanged(value); } }
        public ICommand SaveSettings { get; set; }
        public ICommand LogIn { get; set; }
        public ICommand LogOut { get; set; }
        public ICommand CancelLogIn { get; set; }
        #endregion

        #region Contructor
        public SettingsModel()
        {
            Initialize();
            CheckLogin();
            SaveSettings = new RelayCommand(( param => { ChangeCurrentLanguage(); RegisterInStartup(RunOnBoot); }));
            LogIn = new RelayCommand((param => LogInMethod()));
            LogOut = new RelayCommand((param => LogOutMethod()));
            RestoreSettings();
        }
        #endregion

        #region Methods

        private void RestoreSettings()
        {
            if (API.Client.Client.IsLoggedIn)
            {
                if(Properties.Settings.Default.Country != "")
                    SelectedCountry = CountryList.IndexOf((_CountryList.FirstOrDefault(x => x.Value == Properties.Settings.Default.Country)).Key);
                if(Properties.Settings.Default.Timezone != "")
                    SelectedTimezone = TimezoneList.IndexOf(Properties.Settings.Default.Timezone);
                SelectedLanguage = GetCurrentLanguage(Properties.Settings.Default.Language);
                SelectedFallBackLanguage = GetCurrentLanguage(Properties.Settings.Default.FallbackLanguage);
            }
        }
        private int GetCurrentLanguage(string language)
        {
            switch (language)
            {
                case "en-US":
                    return 0;
                case "ru-Ru":
                    return 1;
                case "lt-LT":
                    return 2;
                default: return 0;
            }
        }

        private void ChangeCurrentLanguage()
        {
            CultureInfo CultureLanguage = null;

            switch (SelectedLanguage)
            {
                case 0:
                    CultureLanguage = new CultureInfo("en-US");
                    Properties.Settings.Default.Language = "en-US";
                    break;
                case 1:
                    CultureLanguage = new CultureInfo("ru-RU");
                    Properties.Settings.Default.Language = "ru-RU";
                    break;
                case 2:
                    CultureLanguage = new CultureInfo("lt-LT");
                    Properties.Settings.Default.Language = "lt-LT";
                    break;
            }
            Properties.Settings.Default.Save();
            Task.Factory.StartNew(API.Configurations.Genre.Load);
            Thread.CurrentThread.CurrentCulture = CultureLanguage;
            Thread.CurrentThread.CurrentUICulture = CultureLanguage;
            NavigationModel.Instance.RemoveControls();
            Window CurrentWindow = Application.Current.MainWindow;
            Application.Current.MainWindow = new MainWindow();
            API.Configurations.Genre.Load();
            Application.Current.MainWindow.Show();

            CurrentWindow.Close();
        }

        private void CheckLogin()
        {
            if (API.Client.Client.IsLoggedIn)
            {
                CurrentStatus = AccountStatus.LoggedIn;
                UserName = API.Account.GetUserSettings().GetAwaiter().GetResult().User.Username;
            }
            else CurrentStatus = AccountStatus.LoggedOut;

        }

        private async void LogInMethod()
        {
            if (CurrentStatus == AccountStatus.LoggedOut)
            {
                API.Authentication.LogInThroughWeb();
                CurrentStatus = AccountStatus.AwaitingCode;
            }
            else if (CurrentStatus == AccountStatus.AwaitingCode && !string.IsNullOrWhiteSpace(PinCode))
            {
                await API.Authentication.GetAuthorizationAsync(PinCode);
                CheckLogin();
            }
            
        }

        private async void LogOutMethod()
        {
            await API.Authentication.RemoveAuthentification();
            CheckLogin();
        }

        private static bool CheckRunOnBoot()
        {
            RegistryKey OurApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (OurApp.GetValue("WatchList") == null)
                return false;
            else
                return true;
        }

        private void LogInVisibilityChanged(AccountStatus input)
        {
            switch (input)
            {
                case AccountStatus.LoggedIn: LoggedInVisibility = true; LogInVisibility = false; AwaitingPinCodeVisibility = false; break;
                case AccountStatus.LoggedOut: LogInVisibility = true; LoggedInVisibility = false; AwaitingPinCodeVisibility = false; break;
                case AccountStatus.AwaitingCode: AwaitingPinCodeVisibility = true; LogInVisibility = false; LoggedInVisibility = false; break;
            }
        }

        private static void RegisterInStartup(bool isChecked)
        {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey
                    ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (isChecked)
                registryKey.SetValue("WatchList", System.Reflection.Assembly.GetEntryAssembly().Location);
            else
                if(registryKey.GetValue("WatchList") != null)
                    registryKey.DeleteValue("WatchList");
        }

        private void Initialize()
        {
            LanguageList = new List<string> { "English", "Russian", "Lithuania" };
            FallBackLanguage = new List<string> { "English", "Russian", "Lithuania" };
            _CountryList = ApiConfiguration.GetCountries();
            OnPropertyChanged("CountryList");
            TimezoneList = ApiConfiguration.GetTimezones().GetAwaiter().GetResult();

            switch (Properties.Settings.Default.Language)
            {
                case "ru-RU":
                    _SelectedLanguage = 1;
                    break;
                case "lt-LT":
                    _SelectedLanguage = 2;
                    break;
                default:
                    _SelectedLanguage = 0;
                    break;
            }
        }

        private void CountryChanged()
        {
            //Console.WriteLine(_CountryList[CountryList[SelectedCountry]]);
            TimezoneList = ApiConfiguration.GetTimezones(_CountryList[CountryList[SelectedCountry+1]]).GetAwaiter().GetResult();
        }
        #endregion
    }
}
