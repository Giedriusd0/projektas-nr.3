﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatchList.Models.Trakt.Calendar;
using WatchList.ViewModels;
using WatchList.API;
using System.Windows;

namespace WatchList.ViewModels
{
    public class CalendarModel : BaseModel
    {
        private List<ICalendar> _CalendarList = null;
        
        public List<ICalendar> CalendarList { get { return _CalendarList; } set { _CalendarList = value; OnPropertyChanged(); } }
        public List<string> WeekDays { get; set; }
        public DateTime today;
        public List<DateTime> Days { get; set; }
        //public DateTime Days { get; set; }

        public CalendarModel()
        {
            WeekDays = new List<string> { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
            today = DateTime.Now;
            Days = new List<DateTime> { };
            //Days = new DateTime();
            for (int i = -7; i <= 7; i++)
            {
                //Days = DateTime.Now.AddDays(i);
                Days.Add(DateTime.Now.AddDays(i));
            }
            SetCallendarList();
            CalendarList.OrderBy(x => x.Released);
        }

        private async void SetCallendarList()
        {
            if (CalendarList == null)
                CalendarList = new List<ICalendar>();
            
            CalendarList.AddRange(await Calendar.GetMyMovies(DateTime.Now.AddDays(-7), 7));
            CalendarList.AddRange(await Calendar.GetMyShows(DateTime.Now.AddDays(-7), 7));
            for (int i = -7; i <= 14; i++)
            {
                TvShow empty = new TvShow() { Episode = null, FirstAired = DateTime.Now.AddDays(i), Show = new Models.Trakt.General.TraktMovieShow() };

                //if (CalendarList)
                if (!CalendarList.Exists(x => x.Released == DateTime.Now.AddDays(i)))
                CalendarList.Add(empty);
                //if (i == today) 
            }

        }
    }
}
