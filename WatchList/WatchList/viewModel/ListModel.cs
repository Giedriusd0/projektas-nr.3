﻿using System.Collections.Generic;
using System.Windows.Input;
using WatchList.API;
using WatchList.Extensions;
using WatchList.Models.General;
using WatchList.Models.Movies;
using System.Linq;
using System.Threading;

namespace WatchList.ViewModels
{
    public class ListModel : BaseModel
    {
        #region Privates
        private bool FirstSeen { get; set; } = true;
        private List<IMovieTvShow> _ItemsSource = new List<IMovieTvShow>();
        private List<Models.Movies.Movie> _MoviesList = new List<Movie>();
        private List<Models.TvShows.TvShow> _TvShowList = new List<Models.TvShows.TvShow>();
        #endregion

        #region Public
        public List<IMovieTvShow> ItemsSource { get { return _ItemsSource; } set { _ItemsSource = value; OnPropertyChanged(); } }
        #endregion

        public ICommand CheckedCommand { get; set; }
        public ICommand UncheckedCommand { get; set; }
        public ICommand RemoveMovieCommand { get; set; }
        public ICommand RemoveShowCommand { get; set; }

        public ListModel()
        {
            CheckedCommand = new RelayCommand(param => ChangeList(MediaType.Movie));
            UncheckedCommand = new RelayCommand(param => ChangeList(MediaType.Tv));
            RemoveMovieCommand = new RelayCommand(param => RemoveMovieFromList((int)param));
            RemoveShowCommand = new RelayCommand(param => RemoveShowFromList((int)param));
            API.Account.WatchListChanged += GetLists;
            GetLists(null, new WatchListArgs(Account.WatchList));
        }
        async void GetLists(object sender, API.WatchListArgs e)
        {
            //Calendar.UpdateCalendar(e.WatchList);
            if (FirstSeen)
            {
                ChangeList(MediaType.Movie);
                FirstSeen = false;
            }
        }
        void ChangeList(MediaType input)
        {
            //SpinWait.SpinUntil(() => Calendar.CalendarUpdated);
            switch (input)
            {
                case MediaType.Movie: ItemsSource = Calendar.CalendarWatchList.FindAll(x => x.MediaType == MediaType.Movie); break;//_MoviesList.ConvertTo(); break;
                case MediaType.Tv: ItemsSource = Calendar.CalendarWatchList.FindAll(x => x.MediaType == MediaType.Tv); break;//_TvShowList.ConvertTo(); break;
            }
        }

        private async void RemoveMovieFromList(int id)
        {
           await Account.RemoveMovieFromWatchList(id);
        }

        private async void RemoveShowFromList(int id)
        {
            await Account.RemoveTvShowFromWatchList(id);
        }
    }
}
