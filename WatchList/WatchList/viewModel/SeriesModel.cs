﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WatchList.Models.General;
using WatchList.Models.Search;
using WatchList.API;
using System.Linq;
using WatchList.Helpers;
using WatchList.Extensions;
using System.Windows.Media.Imaging;
using System.Windows.Controls;

namespace WatchList.ViewModels
{
    public class SeriesModel : BaseModel
    {
        #region Privates
        private List<string> _YearsSource = GeneralHelpers.GenerateYears(100);
        private int _YearsSelected = 0;
        private List<string> _SortBySource = Enum.GetNames(typeof(Discover.SortBy)).ToList();
        private int _SortBySelected = 1;
        private ObservableCollection<string> _GenreSource = new ObservableCollection<string>();
        private ObservableCollection<string> _GenreSelected = new ObservableCollection<string>();
        private string _GenreTextInput = "";
        private bool _GenreListVisibility = false;
        private bool _IsGridFocused = true;
        private SearchContainer<SearchTv> _SeriesList;
        private BitmapImage testimage;
        private int _currentPage = 1;
        private ScrollViewer _Scroller;
        #endregion

        #region Public
        public List<string> YearsSource { get { return _YearsSource; } set { _YearsSource = value; OnPropertyChanged(); } }
        public int YearsSelected { get { return _YearsSelected; } set { _YearsSelected = value; RefreshList(); OnPropertyChanged(); } }
        public List<string> SortBySource { get { return _SortBySource; } set { _SortBySource = value; OnPropertyChanged(); } }
        public int SortBySelected { get { return _SortBySelected; } set { _SortBySelected = value; RefreshList(); OnPropertyChanged(); } }
        public ObservableCollection<string> GenreSourceFiltered { get { return (from genre in _GenreSource where genre.StartsWith(GenreTextInput, true, null) select genre).ToObservableCollection<string>(); } set { { _GenreSource = value; /*NotifyPropertyChanged("_GenreSource");*/ } } }
        public ObservableCollection<string> GenreSelected { get { return _GenreSelected; } set { _GenreSelected = value; OnPropertyChanged(); } }
        public string GenreTextInput { get { return _GenreTextInput; } set { _GenreTextInput = value; OnPropertyChanged(); NotifyPropertyChanged("GenreSourceFiltered"); } }
        public bool GenreListVisibility { get { return _GenreListVisibility; } set { _GenreListVisibility = value; OnPropertyChanged(); } }
        public bool IsGridFocused { get { return _IsGridFocused; } set { _IsGridFocused = value; OnPropertyChanged(); } }
        public SearchContainer<SearchTv> SeriesList { get { return _SeriesList; } set { _SeriesList = value; OnPropertyChanged(); } }
        public BitmapImage TestImage { get { return testimage; } set { testimage = value; OnPropertyChanged(); } }
        public int CurrentPage { get { return _currentPage; } set { _currentPage = value; OnPropertyChanged(); RefreshList(); } }
        #endregion

        #region Commands
        public ICommand GenreAddToSelected { get; set; }
        public ICommand GenreRemoveFromSelected { get; set; }
        public ICommand GenreListVisibilityChange { get; set; }
        public ICommand FocusGrid { get; set; }
        public ICommand ChangePage { get; set; }
        public ICommand AddToWatchList { get; set; }
        #endregion

        #region Constructor
        public SeriesModel(ScrollViewer scroller, string genre = "")
        {
            GenreAddToSelected = new RelayCommand(param => GenreAddToSelectedMethod((int)param));
            GenreRemoveFromSelected = new RelayCommand(param => GenreRemoveFromSelectedMethod((int)param));
            GenreListVisibilityChange = new RelayCommand(param => GenreListVisibility = Convert.ToBoolean(param));
            FocusGrid = new RelayCommand(param => TempMethod());
            ChangePage = new RelayCommand(param => ChangePageMethod((string)param));
            AddToWatchList = new RelayCommand(param => AddToWatchListMethod((int)param));
            _Scroller = scroller;
            API.Configurations.Genre.GenreUpdated += Genre_GenreUpdated;
            Genre_GenreUpdated(null, new API.Configurations.GenreEventArgs(null, API.Configurations.Genre.TvGenres));
            if(!string.IsNullOrEmpty(genre))
                GenreAddToSelectedMethod(genre);
            RefreshList();
        }

        private void Genre_GenreUpdated(object sender, API.Configurations.GenreEventArgs e)
        {
            _GenreSource = new ObservableCollection<string>(GeneralHelpers.ToStringColl(e.TvGenres.Genres));
        }
        #endregion

        #region Methods
        void GenreAddToSelectedMethod(int index)
        {
            if (index >= 0)
            {
                GenreSelected.Add(_GenreSource[index]);
                _GenreSource.RemoveAt(index);
            }
            NotifyPropertyChanged("GenreSourceFiltered");
            RefreshList();
        }
        void GenreAddToSelectedMethod(string input)
        {
            for (int i = 0; i < _GenreSource.Count; i++)
            {
                if (_GenreSource[i] == input)
                {
                    GenreSelected.Add(_GenreSource[i]);
                    _GenreSource.RemoveAt(i);
                    break;
                }
            }
        }
        void GenreRemoveFromSelectedMethod(int index)
        {
            if (index >= 0)
            {
                _GenreSource.Add(GenreSelected[index]);
                GenreSelected.RemoveAt(index);
            }
            NotifyPropertyChanged("GenreSourceFiltered");
            RefreshList();
        }
        async void RefreshList()
        {
            if (YearsSelected > 0)
                SeriesList = await Discover.Instance.GetDiscoveryTvShowList((Discover.SortBy)SortBySelected, GenreSelected.ToList(), Convert.ToInt32(YearsSource[YearsSelected]), CurrentPage);
            else
                SeriesList = await Discover.Instance.GetDiscoveryTvShowList((Discover.SortBy)SortBySelected, GenreSelected.ToList(), page: CurrentPage);
        }
        void GetGenres()
        {
            _GenreSource = new ObservableCollection<string>(GeneralHelpers.ToStringColl(API.Configurations.Genre.TvGenres.Genres));
        }
        async void AddToWatchListMethod(int id)
        {
            await API.Account.AddTvShowToWatchList(id);
            //await API.Account.UpdateWatchList();
            RefreshList();
        }
        // TODO: GenreInputTextBox should lose focus when clicked on grid
        void ChangePageMethod(string input)
        {
            if (input == "Next")
            {
                if (CurrentPage != SeriesList.TotalPages)
                {
                    CurrentPage++;
                }
            }
            else if (input == "Previous")
            {
                if (CurrentPage != 1)
                {
                    CurrentPage--;
                }
            }
            _Scroller.ScrollToTop();
        }
        void TempMethod()
        {
            IsGridFocused = true;
        }
        #endregion
    }
}
