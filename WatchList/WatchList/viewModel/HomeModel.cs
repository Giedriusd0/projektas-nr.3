﻿using System.Collections.Generic;
using System.Windows.Input;
using WatchList.Models.General;
using WatchList.Models.Search;
using WatchList.API;
using WatchList.Extensions;

namespace WatchList.ViewModels
{
    public class HomeModel : BaseModel
    {
        #region Privates
        private string _SearchInput;
        private SearchContainer<ISearchMovieTvInterface> _PopularMovieList;
        private SearchContainer<ISearchMovieTvInterface> _TopRatedMovieList;
        private SearchContainer<ISearchMovieTvInterface> _PopularTvList;
        private SearchContainer<ISearchMovieTvInterface> _TopRatedTvList;
        private SearchContainer<ISearchMovieTvInterface> _CurrentList;
        private List<Models.Search.SearchBase> _SearchResult;
        #endregion

        #region Public
        public SearchContainer<ISearchMovieTvInterface> CurrentList { get { return _CurrentList; } set { _CurrentList = value; OnPropertyChanged(); } }
        public string SearchInput { get { return _SearchInput; } set { _SearchInput = value; QuickSearch(); } }
        public bool SearchResultVisibilyti { get; set; } = false;
        public List<Models.Search.SearchBase> SearchResult { get { return _SearchResult; } set { _SearchResult = value; if (value != null) SearchResultVisibilyti = true; else SearchResultVisibilyti = false; OnPropertyChanged("SearchResultVisibilyti"); OnPropertyChanged(); } }
        public ICommand ChangeListTo { get; set; }
        public ICommand ShowMovie { get; set; }
        public ICommand ShowSeries { get; set; }
        public ICommand SearchSelected { get; set; }
        #endregion

        #region Constructor
        public HomeModel()
        {
            ChangeListTo = new RelayCommand(param => ChangeCurrentList(param.ToString()));
            SearchSelected = new RelayCommand(param => SearchSelectedMethod(param));
            GetLists();
        }
        #endregion

        #region Methods
        private async void QuickSearch()
        {
            if (!string.IsNullOrWhiteSpace(SearchInput))
            {
                Models.General.SearchContainer<Models.Search.SearchBase> QuickSearch = await API.Search.Instance.SearchMultiAsync(SearchInput);
                if (QuickSearch != null && QuickSearch.QueryText == SearchInput)
                    SearchResult = QuickSearch.Results;
            }
            else SearchResult = null;
        }
        private async void GetLists()
        {
            _PopularMovieList = (await Movies.Instance.GetMoviePopularListAsync()).ConvertTo();
            _TopRatedMovieList = (await Movies.Instance.GetMovieTopRatedListAsync()).ConvertTo();
            _PopularTvList = (await TvShows.Instance.GetTvShowPopularAsync()).ConvertTo();
            _TopRatedTvList = (await TvShows.Instance.GetTvShowTopRatedAsync()).ConvertTo();
            CurrentList = _PopularMovieList;
        }
        private void ChangeCurrentList(string list)
        {
            switch (list)
            {
                case "PopularMovies": CurrentList = _PopularMovieList; break;
                case "TopRatedMovies": CurrentList = _TopRatedMovieList; break;
                case "PopularTv": CurrentList = _PopularTvList; break;
                case "TopRatedTv": CurrentList = _TopRatedTvList; break;
            }
        }
        private void SearchSelectedMethod(object selectedItem)
        {
            switch (selectedItem.GetType().ToString())
            {
                case "WatchList.Models.Search.SearchMovie":
                    {
                        NavigationModel.Instance.ShowMovieMethod((selectedItem as SearchMovie).Id); break;
                    }
                case "WatchList.Models.Search.SearchTv":
                    {
                        NavigationModel.Instance.ShowSeriesMethod((selectedItem as SearchTv).Id); break;
                    }
            }
        }
        #endregion
    }
}
