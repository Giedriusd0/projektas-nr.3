﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Animation;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WatchList.Views;
using System.Globalization;
using System.Threading;

namespace WatchList.ViewModels
{
    public class NavigationModel : BaseModel
    {
        #region Privates
        private UserControl _PreviousView;
        private UserControl _CurrentView;
        // Views
        private HomeControl _HomeView = null;
        private MoviesControl _MoviesView = null;
        private SeriesControl _SeriesView = null;
        private SettingsControl _SettingsView = null;
        private ListControl _ListView = null;
        private CalendarControl _CalendarView = null;
        private ShowMovieControl _ShowMovieView = null;
        private ShowSeriesControl _ShowSeriesView = null;
        // Animations
        private static Storyboard SlideOutUserControl = Application.Current.Resources["SlideOutUserControl"] as Storyboard;
        private static Storyboard SlideInUserControl = Application.Current.Resources["SlideInUserControl"] as Storyboard;
        private static Storyboard SlideInSubNavigation = Application.Current.Resources["SlideInSubNavigation"] as Storyboard;
        private static Storyboard SlideOutSubNavigation = Application.Current.Resources["SlideOutSubNavigation"] as Storyboard;
        //
        private Thickness _SubNavigatonClosed = new Thickness(7, 2, 7, 80);
        private Thickness _SubNavigationOpened = new Thickness(7, 2, 7, 0);
        private Thickness _BrowseSubNavigationThickness;
        private Thickness _MyListSubNavigationThickness;
        #endregion

        #region Public
        public UserControl CurrentView { get { return _CurrentView; } set { _CurrentView = value; OnPropertyChanged(); } }
        public Thickness BrowseSubNavigationThickness { get { return _BrowseSubNavigationThickness; } set { _BrowseSubNavigationThickness = value; OnPropertyChanged(); } }
        public Thickness MyListSubNavigationThickness { get { return _MyListSubNavigationThickness; } set { _MyListSubNavigationThickness = value; OnPropertyChanged(); } }
        public enum View { Home, Movies, Series, Settings, List, Calendar }
        #endregion

        #region Commands
        public ICommand GoTo { get; set; } = new RelayCommand(param => Instance.NavigateTo((View)Enum.Parse(typeof(View), param.ToString())));
        public ICommand GoBack { get; set; } = new RelayCommand(param => Instance.GoBackMethod());
        public ICommand ShowMovie { get; set; } = new RelayCommand(param => Instance.ShowMovieMethod((int)param));
        public ICommand ShowSeries { get; set; } = new RelayCommand(param => Instance.ShowSeriesMethod((int)param));
        #endregion

        #region Instance
        private static NavigationModel ThisObject = null;
        public static NavigationModel Instance { get { if (ThisObject == null) ThisObject = new NavigationModel(); return ThisObject; } private set { ThisObject = value; } }
        #endregion

        #region Constructor
        public NavigationModel()
        {
            RestoreDefaultLanguage();
            CurrentView = new Views.HomeControl();
            BrowseSubNavigationThickness = _SubNavigatonClosed;
            MyListSubNavigationThickness = _SubNavigatonClosed;
        }
        #endregion

        #region Methods
        public void NavigateTo(View view)
        {
            switch (view)
            {
                case View.Home:
                    if (_HomeView == null)
                        _HomeView = new HomeControl() { Name = "HomeControl", Margin = new Thickness(2000, 0, 0, 0) };
                    SwitchView(_HomeView); break;
                case View.Calendar:
                    if (_CalendarView == null && API.Client.Client.IsLoggedIn)
                    {
                        _CalendarView = new CalendarControl() { Name = "CalendarControl", Margin = new Thickness(2000, 0, 0, 0) };
                        SwitchView(_CalendarView);
                    } break;
                case View.List:
                    if (_ListView == null && API.Client.Client.IsLoggedIn)
                    {
                        _ListView = new ListControl() { Name = "ListControl", Margin = new Thickness(2000, 0, 0, 0) };
                        SwitchView(_ListView);
                    } break;
                case View.Settings:
                    if (_SettingsView == null)
                        _SettingsView = new SettingsControl() { Name = "SettingsControl", Margin = new Thickness(2000, 0, 0, 0) };
                    SwitchView(_SettingsView); break;
                case View.Series:
                    if (_SeriesView == null)
                        _SeriesView = new SeriesControl() { Name = "SeriesControl", Margin = new Thickness(2000, 0, 0, 0) };
                    SwitchView(_SeriesView); break;
                case View.Movies:
                    if (_MoviesView == null)
                        _MoviesView = new MoviesControl() { Name = "MoviesControl", Margin = new Thickness(2000, 0, 0, 0) };
                    SwitchView(_MoviesView); break;
            }
        }
        public void ShowMovieMethod(int id)
        {
            _PreviousView = CurrentView;
            CurrentView = new ShowMovieControl(id);
        }
        public void ShowSeriesMethod(int id)
        {
            _PreviousView = CurrentView;
            CurrentView = new ShowSeriesControl(id);
        }
        private void GoBackMethod()
        {
            CurrentView = _PreviousView;
        }
        private void SwitchView(UserControl input)
        {
            if (CurrentView != input)
            {
                // Control sub navigation opened or closed
                switch (input.Name)
                {
                    case "MoviesControl":
                    case "SeriesControl":
                        BrowseSubNavigationThickness = _SubNavigationOpened;
                        MyListSubNavigationThickness = _SubNavigatonClosed;
                        break;
                    case "ListControl":
                    case "CalendarControl":
                        MyListSubNavigationThickness = _SubNavigationOpened;
                        BrowseSubNavigationThickness = _SubNavigatonClosed;
                        break;
                    default:
                        MyListSubNavigationThickness = _SubNavigatonClosed;
                        BrowseSubNavigationThickness = _SubNavigatonClosed;
                        break;
                }

                _PreviousView = CurrentView;
                input.BeginStoryboard(SlideInUserControl);
                CurrentView = input;
                if (_PreviousView != null)
                    _PreviousView.BeginStoryboard(SlideOutUserControl);
            }
        }
        private void DiscoverByGenreMovies(string genre)
        {
            SwitchView(new MoviesControl(genre));
        }
        private void DiscoverByGenreSeries(string genre)
        {
            SwitchView(new SeriesControl(genre));
        }
        public void RemoveControls()
        {
            Instance = new NavigationModel();
        }
        private static void RestoreDefaultLanguage()
        {
            CultureInfo CultureLanguage = new CultureInfo(Properties.Settings.Default.Language);
            Thread.CurrentThread.CurrentCulture = CultureLanguage;
            Thread.CurrentThread.CurrentUICulture = CultureLanguage;
        }
        #endregion
    }
}
