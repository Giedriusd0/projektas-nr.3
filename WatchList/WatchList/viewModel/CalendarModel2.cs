﻿using System;
using System.Collections.Generic;
using WatchList.Models.General;
using System.Linq;
using WatchList.Models.TvShows;
using WatchList.Models.Movies;
using System.Threading.Tasks;
using System.Threading;

namespace WatchList.ViewModels
{
    public class CalendarModel2 : BaseModel
    {
        private DateTime _SelectedDate;
        private List<Day> _MonthList;
        private int[] _YearSource;
        private int _SelectedYear = 1;
        private int[] _MonthSource;
        private int _SelectedMonth = 1;

        public DateTime SelectedDate { get { return _SelectedDate; } set { _SelectedDate = value; OnPropertyChanged(); } }
        public List<Day> MonthList { get { return _MonthList; } set { _MonthList = value; OnPropertyChanged(); } }
        public int[] YearSource { get { return _YearSource; } set { _YearSource = value; OnPropertyChanged(); } }
        public int[] MonthSource { get { return _MonthSource; } set { _MonthSource = value; OnPropertyChanged(); } }
        public int SelectedYear { get { return _SelectedYear; } set { _SelectedYear = value; OnPropertyChanged(); } }
        public int SelectedMonth { get { return _SelectedMonth; } set { _SelectedMonth = value; OnPropertyChanged(); } }

        public CalendarModel2()
        {
            SelectedDate = DateTime.Now;
            YearSource = new int[] { 2017, 2018, 2019, 2020 };
            MonthSource = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
            CreateMonthList();
            var temp = API.Calendar.CalendarWatchList;
        }

        private async void CreateMonthList()
        {
            MonthList = new List<Day>();
            SpinWait.SpinUntil(() => API.Calendar.CalendarUpdated);

            for (int i = 1; i <= DateTime.DaysInMonth(SelectedDate.Year, SelectedDate.Month); i++)
            {
                Day temp = new Day(i);
                if (temp.Events == null)
                    temp.Events = new List<IMovieTvShow>();
                IMovieTvShow iMovieTvShow = API.Calendar.CalendarWatchList.FirstOrDefault(x => x.ReleaseDate?.Year == SelectedDate.Year && x.ReleaseDate?.Month == SelectedDate.Month && x.ReleaseDate?.Day == i);
                if(iMovieTvShow !=null)
                    temp.Events.Add(iMovieTvShow);

                MonthList.Add(temp);
            }
        }
    }
}
