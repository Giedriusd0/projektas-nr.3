﻿using System.Windows.Input;
using WatchList.Models.TvShows;
using WatchList.API;
using Vlc.DotNet.Wpf;
using System.Threading.Tasks;
using System.IO;
using System;
using WatchList.Services;

namespace WatchList.ViewModels
{
    public class ShowSeriesModel : BaseModel
    {
        #region Privates
        private TvShow _CurrentTvShow;
        private bool? _isInList;
        private bool _ShowTrailer;
        private VlcVideoSourceProvider _Player;
        #endregion

        #region Public
        public TvShow CurrentTvShow { get { return _CurrentTvShow; } set { _CurrentTvShow = value; OnPropertyChanged(); } }
        public bool? IsInWatchList { get { return _isInList; } set { _isInList = value; OnPropertyChanged(); } }
        public bool ShowTrailer { get { return _ShowTrailer; } set { _ShowTrailer = value; OnPropertyChanged(); } }
        public int PlayerVolume { get { if (_Player.MediaPlayer != null) return _Player.MediaPlayer.Audio.Volume; else return 100; } set { _Player.MediaPlayer.Audio.Volume = value; OnPropertyChanged(); } }
        public ICommand AddToWatchList { get; set; }
        public ICommand WatchTrailer { get; set; }
        public ICommand VideoPlay { get; set; }
        public ICommand VideoPause { get; set; }
        public ICommand VideoExit { get; set; }
        public ICommand VideoMute { get; set; }
        #endregion


        #region Constructor
        public ShowSeriesModel(int id, VlcVideoSourceProvider vlc)
        {
            _Player = vlc;
            AddToWatchList = new RelayCommand(param => AddToWatchListMethod());
            WatchTrailer = new RelayCommand(param => { if (ShowTrailer == false) ShowTrailer = true; else ShowTrailer = false; });
            VideoPlay = new RelayCommand(param => _Player.MediaPlayer.Play());
            VideoPause = new RelayCommand(param => _Player.MediaPlayer.Pause());
            VideoExit = new RelayCommand(param => { _Player.MediaPlayer.Pause(); ShowTrailer = false; });
            VideoMute = new RelayCommand(param => _Player.MediaPlayer.Audio.ToggleMute());

            CheckIfIsInList(id);
            SeriesChanged(id);
        }
        #endregion

        #region Methods
        private async void SeriesChanged(int id)
        {
            CurrentTvShow = await TvShows.Instance.GetTvShowAsync(id);
            Task.Run(async () =>
            {
                await InitializePlayer();
            });
            Task.Run(async () =>
            {
                await GetTrailer();
            });
        }
        async void AddToWatchListMethod()
        {
            await API.Account.AddTvShowToWatchList(CurrentTvShow.Id);
            //await API.Account.UpdateWatchList();
            CheckIfIsInList(CurrentTvShow.Id);
        }
        void CheckIfIsInList(long id)
        {
            IsInWatchList = API.Account.IsInWatchList((int)id);
        }
        async Task GetTrailer()
        {
            var temp = await API.TvShows.Instance.GetTvShowVideosAsync(CurrentTvShow.Id);
            _Player.MediaPlayer.SetMedia(Youtube.Instance.GetVideoUrl(temp.Results[0].Key).Url);
        }
        async Task InitializePlayer()
        {
            _Player.CreatePlayer(new DirectoryInfo(System.IO.Path.Combine(Environment.CurrentDirectory, "Resources/Vlc/x86")));
        }
        #endregion
    }
}
