﻿using System.Windows.Input;
using WatchList.Models.Movies;
using WatchList.API;
using Vlc.DotNet.Wpf;
using WatchList.Services;
using System.Threading.Tasks;
using System.IO;
using System;

namespace WatchList.ViewModels
{
    public class ShowMovieModel : BaseModel
    {
        #region Privates
        private bool? _IsInList;
        private bool _ShowTrailer;
        private Movie _CurrentMovie;
        private VlcVideoSourceProvider _Player;
        #endregion

        #region Public
        public bool? IsInWatchList { get { return _IsInList; } set { _IsInList = value; OnPropertyChanged(); } }
        public Movie CurrentMovie { get { return _CurrentMovie; } private set { _CurrentMovie = value; OnPropertyChanged(); } }
        public bool ShowTrailer { get { return _ShowTrailer; } set { _ShowTrailer = value; OnPropertyChanged(); } }
        public int PlayerVolume { get { if (_Player.MediaPlayer != null) return _Player.MediaPlayer.Audio.Volume; else return 100; } set { if(_Player.MediaPlayer != null)_Player.MediaPlayer.Audio.Volume = value; OnPropertyChanged(); } }
        public ICommand GoBack { get; set; }

        public ICommand AddToWatchList { get; set; }
        public ICommand DiscoverByGenre { get; set; }
        public ICommand WatchTrailer { get; set; }
        public ICommand VideoPlay { get; set; }
        public ICommand VideoPause { get; set; }
        public ICommand VideoExit { get; set; }
        public ICommand VideoMute { get; set; }
        #endregion

        #region Constructor
        public ShowMovieModel(long id, VlcVideoSourceProvider vlc)
        {
            _Player = vlc;
            AddToWatchList = new RelayCommand(param => AddToWatchListMethod());
            WatchTrailer = new RelayCommand(param => { if (ShowTrailer == false) ShowTrailer = true; else ShowTrailer = false; });
            VideoPlay = new RelayCommand(param => { if (_Player.MediaPlayer != null) _Player.MediaPlayer.Play(); });
            VideoPause = new RelayCommand(param => { if (_Player.MediaPlayer != null) _Player.MediaPlayer.Pause(); });
            VideoExit = new RelayCommand(param => { if(_Player.MediaPlayer !=null) _Player.MediaPlayer.Pause(); ShowTrailer = false; });
            VideoMute = new RelayCommand(param => { if (_Player.MediaPlayer != null) _Player.MediaPlayer.Audio.ToggleMute(); });
            CheckIfIsInList(id);
            MovieChanged(id);
        }
        #endregion

        #region Methods
        private async void MovieChanged(long id)
        {
            CurrentMovie = await Movies.Instance.GetMovieAsync((int)id);
            //IsInWatchList
            Task.Run(async () =>
            {
                await InitializePlayer();
            });
            Task.Run(async () =>
            {
                await GetTrailer();
            });
        }
        async void AddToWatchListMethod()
        {
            await API.Account.AddMovieToWatchList(CurrentMovie.Id);
            //await API.Account.UpdateWatchList();
            CheckIfIsInList(CurrentMovie.Id);
        }

        void CheckIfIsInList(long id)
        {
            IsInWatchList = API.Account.IsInWatchList((int)id);
        }
        async Task GetTrailer()
        {
            var temp = await API.Movies.Instance.GetMovieVideosAsync(CurrentMovie.Id);
            _Player.MediaPlayer.SetMedia(Youtube.Instance.GetVideoUrl(temp.Results[0].Key).Url);
        }
        async Task InitializePlayer()
        {
             _Player.CreatePlayer(new DirectoryInfo(System.IO.Path.Combine(Environment.CurrentDirectory, "Resources/Vlc/x86")));
        }
        #endregion
    }
}