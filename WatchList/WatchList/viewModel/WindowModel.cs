﻿using System;
using System.Windows;
using System.Windows.Input;

namespace WatchList.ViewModels
{
    public class WindowModel : BaseModel
    {
        #region Private properties
        private int _WindowHeight = 800;
        private int _WindowWidth = 1000;
        private int _MinWindowHeight = 600;
        private int _MinWindowWidth = 800;
        private int _OuterBorderRadius = 10;
        private bool Maximized = false;
        private int MaxWindowHeight = (int)SystemParameters.WorkArea.Height;
        private int MaxWindowWidth = (int)SystemParameters.WorkArea.Width;
        private Window AppWindow;
        #endregion

        #region Public properties
        public ICommand MinimizeCommand { get; set; }
        public ICommand MaximizeCommand { get; set; }
        public ICommand CloseCommand { get; set; }
        public ICommand DragToMove { get; set; }
        public int WindowHeight { get { return _WindowHeight; } set { _WindowHeight = value; OnPropertyChanged(); } }
        public int WindowWidth { get { return _WindowWidth; } set { _WindowWidth = value; OnPropertyChanged(); } }
        public int MinWindowHeight { get { return _MinWindowHeight; } set { } }
        public int MinWindowWidth { get { return _MinWindowWidth; } set { } }
        public int OuterBorderRadius { get { return _OuterBorderRadius; } set { _OuterBorderRadius = value; OnPropertyChanged(); } }
        #endregion

        public WindowModel(Window window)
        {
            AppWindow = window;

            MinimizeCommand = new RelayCommand((param => window.WindowState = WindowState.Minimized));
            MaximizeCommand = new RelayCommand(param => ToggleWindowSize());
            CloseCommand = new RelayCommand((param => Application.Current.Shutdown()));
            DragToMove = new RelayCommand(param => AppWindow.DragMove());
        }

        private void ToggleWindowSize()
        {
            if(Maximized == true)
            {
                AppWindow.Height = 800;
                AppWindow.Width = 1000;
                AppWindow.Top = (SystemParameters.WorkArea.Height - AppWindow.Height) / 2;
                AppWindow.Left = (SystemParameters.WorkArea.Width - AppWindow.Width) / 2;
                OuterBorderRadius = 10;
                Maximized = false;
            } else if(Maximized == false)
            {
                AppWindow.Top = 0;
                AppWindow.Left = 0;
                AppWindow.Width = MaxWindowWidth;
                AppWindow.Height = MaxWindowHeight;
                OuterBorderRadius = 0;
                Maximized = true;
            } else
            {
                MessageBox.Show("A bool is not yay nor nay. What?");
            }
        }
        public T GetOne<T>(object o)
        {
            T one = (T)Enum.Parse(typeof(T), o.ToString());
            return one;
        }

    }
}
