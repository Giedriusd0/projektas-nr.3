﻿using System.Windows;
using System.Windows.Input;

namespace WatchList.Views
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //NavigationModel.InitializeNavigation(UserControlView, BrowseSubNavigation, MyListSubNavigation);
            this.DataContext = new ViewModels.WindowModel(this);
        }

        private void Grid_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.F1)
            {
                Window player = new Window
                { Content = new TrailerControl() };
                player.Show();
            }
            if(e.Key == Key.F2)
            {
                MessageBox.Show(CurrentView.Content.ToString());
            }
        }
    }
}
