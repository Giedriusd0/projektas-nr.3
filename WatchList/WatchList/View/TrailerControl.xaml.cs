﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WatchList.Services;
using Vlc.DotNet;
using Vlc.DotNet.Wpf;

namespace WatchList.Views
{
    /// <summary>
    /// Interaction logic for TrailerControl.xaml
    /// </summary>
    public partial class TrailerControl : UserControl
    {
        public TrailerControl()
        {
            InitializeComponent();
            TempMethod();

            DirectoryInfo vlcLibDirectory;
            if (IntPtr.Size == 4)
            {
                // Use 32 bits library
                vlcLibDirectory = new DirectoryInfo(System.IO.Path.Combine(Environment.CurrentDirectory, "Resources/Vlc/x86"));
            }
            else
            {
                // Use 64 bits library
                vlcLibDirectory = new DirectoryInfo(System.IO.Path.Combine(Environment.CurrentDirectory, "Resources/Vlc/x64"));
            }
            MyControl.SourceProvider.CreatePlayer(vlcLibDirectory);

            // Load libvlc libraries and initializes stuff. It is important that the options (if you want to pass any) and lib directory are given before calling this method.
            //this.MyControl.SourceProvider.MediaPlayer.Play("https://r1---sn-h8u8-8ovl.googlevideo.com/videoplayback?ip=5.20.200.107&mime=video%2Fmp4&pl=19&mt=1516901117&source=youtube&dur=143.243&mv=m&ms=au&mm=31&ipbits=0&mn=sn-h8u8-8ovl&id=o-AIQAybmnGRN1RYETe-SoSYcqkjr19b-OrqgRGKs5Zzua&key=yt6&initcwndbps=1543750&ei=cRNqWuKAAprkyQXcmIOoBQ&expire=1516922833&sparams=dur%2Cei%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpl%2Cratebypass%2Crequiressl%2Csource%2Cexpire&requiressl=yes&lmt=1507186886378336&itag=22&signature=99885DC81ABD54FA5A4F8CC0DB668251FD350C50.4D4B9710E5AFB1294CBFCA2B77F54E4EC93C99AF&ratebypass=yes");//"http://download.blender.org/peach/bigbuckbunny_movies/big_buck_bunny_480p_h264.mov");
            //TempMethod();


        }
        async void TempMethod()
        {
            Console.WriteLine("Get Discovery list.");
            var tempList = await API.Discover.Instance.GetDiscoveryMovieList();
            Console.WriteLine("Discovery list got. Getting videos.");
            var tempList2 = await API.Movies.Instance.GetMovieVideosAsync(tempList.Results[0].Id);
            Console.WriteLine("Videos got. Getting youtube link.");
            var tempLink = Youtube.Instance.GetVideoUrl(Youtube.Quality.HD720, tempList2.Results[0].Key);
            Console.WriteLine("Settings source.");
            MyControl.SourceProvider.MediaPlayer.Play(tempLink.Url);
            //MyControl.SourceProvider.MediaPlayer.EncounteredError += MediaPlayer_EncounteredError;
        }

        private void MediaPlayer_EncounteredError(object sender, Vlc.DotNet.Core.VlcMediaPlayerEncounteredErrorEventArgs e)
        {
            //throw new NotImplementedException();
        }
    }
}
