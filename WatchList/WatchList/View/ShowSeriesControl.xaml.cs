﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WatchList.ViewModels;

namespace WatchList.Views
{
    /// <summary>
    /// Interaction logic for ShowSeriesControl.xaml
    /// </summary>
    public partial class ShowSeriesControl : UserControl
    {
        public ShowSeriesControl(int id)
        {
            InitializeComponent();
            this.DataContext = new ShowSeriesModel(id, vlcPlayer.SourceProvider);
        }
    }
}
