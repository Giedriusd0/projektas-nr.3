﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using WatchList.ViewModels;

namespace WatchList.Views
{
    /// <summary>
    /// Interaction logic for CalendarControl.xaml
    /// </summary>
    public partial class CalendarControl : UserControl
    {
       public CalendarControl()
        {
            InitializeComponent();
            this.Margin = new Thickness(2000, 0, 0, 0);
            DataContext = new CalendarModel();
            //List<string> months = new List<string> { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            //ChooseMonth.ItemsSource = months;

            //for (int i = -5; i < 5; i++)
            //{
            //    ChooseYear.Items.Add(DateTime.Today.AddYears(i).Year);
            //}

            //ChooseMonth.SelectedItem = months.FirstOrDefault(w => w == DateTime.Today.ToString("MMMM"));
            //ChooseYear.SelectedItem = DateTime.Today.Year;
        }       
    }
}
