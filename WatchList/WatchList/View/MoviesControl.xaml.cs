﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WatchList.ViewModels;

namespace WatchList.Views
{
    /// <summary>
    /// Interaction logic for TvControl.xaml
    /// </summary>
    public partial class MoviesControl : UserControl
    {
        public MoviesControl()
        {
            InitializeComponent();
            this.DataContext = new MoviesModel(scroller);
        }
        public MoviesControl(string genre)
        {
            InitializeComponent();
            this.DataContext = new MoviesModel(scroller, genre);
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            (sender as Grid).Focus();
        }
    }
}
