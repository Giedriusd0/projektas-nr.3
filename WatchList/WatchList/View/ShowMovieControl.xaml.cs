﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using WatchList.ViewModels;

namespace WatchList.Views
{
    /// <summary>
    /// Interaction logic for ShowMovieControl.xaml
    /// </summary>
    public partial class ShowMovieControl : UserControl
    {
        public ShowMovieControl(int id)
        {
            InitializeComponent();
            this.DataContext = new ShowMovieModel(id, vlcPlayer.SourceProvider);
        }
    }
}
