﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatchList.Models.Movies;
using WatchList.Models.Trakt.WatchList;
using WatchList.Models.TvShows;

namespace WatchList.Extensions
{
    public static class UserMovieShowExtension
    {
        public static List<IMovieTvShow> ConvertTo (this List<Movie> input)
        {
            List<IMovieTvShow> temp = new List<IMovieTvShow>();
            foreach (var item in input)
                temp.Add(item);
            return temp;
        }
        public static List<IMovieTvShow> ConvertTo(this List<TvShow> input)
        {
            List<IMovieTvShow> temp = new List<IMovieTvShow>();
            foreach (var item in input)
                temp.Add(item);
            return temp;
        }
    }
}
