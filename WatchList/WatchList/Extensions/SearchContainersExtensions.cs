﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WatchList.Models.General;
using WatchList.Models.Search;

namespace WatchList.Extensions
{
    public static class SearchContainersExtensions
    {
        public static SearchContainer<ISearchMovieTvInterface> ConvertTo (this SearchContainer<SearchMovie> input)
        {
            if (input != null)
            {
                SearchContainer<ISearchMovieTvInterface> temp = new SearchContainer<ISearchMovieTvInterface>();

                temp.QueryText = input.QueryText;
                temp.Page = input.Page;
                temp.TotalPages = input.TotalPages;
                temp.TotalResults = input.TotalResults;
                temp.Results = new List<ISearchMovieTvInterface>();

                if(input.Results != null)
                foreach (var item in input.Results)
                {
                    temp.Results.Add(item as ISearchMovieTvInterface);
                }

                return temp;
            }
            return null;
        }
        public static SearchContainer<ISearchMovieTvInterface> ConvertTo (this SearchContainer<SearchTv> input)
        {
            if (input != null)
            {
                SearchContainer<ISearchMovieTvInterface> temp = new SearchContainer<ISearchMovieTvInterface>();

                temp.QueryText = input.QueryText;
                temp.Page = input.Page;
                temp.TotalPages = input.TotalPages;
                temp.TotalResults = input.TotalResults;
                temp.Results = new List<ISearchMovieTvInterface>();

                if(input.Results != null)
                foreach (var item in input.Results)
                {
                    temp.Results.Add(item as ISearchMovieTvInterface);
                }
                return temp;
            }
            return null;
        }
    }
}
