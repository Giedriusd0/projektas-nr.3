MINE MINE MINE

1) Discovery Movies
	- Year filter (Just a combo box should do)
	- Genre filter (What the...?)
	- SortBy (Another combobox)
	- Title, year, genres, overview, poster
	- Ratings?
2) Discovery Series
	- Copy and paste from movies, just remember to change bindings
3) ShowMovie/ShowSeries Player
	- VLC player extension
	- VLC player style (probably in spererate usercontrol?)
	- VLC player lightweight
	- Paths instead of images/text would be nice
	- Fullscreen somehow
4) ShowMovie/Series add to list (probably WatchList)
	- Rating somewhere
5) XAML check, clean and themes
	- Dynamic resources EVERYWHERE
	- Proper naming would also be good
	- Maybe add some skewing to navigation buttons, or more path? Path's suck.
6) Notifications 
	- Dunno, google for inspiration?
7) Redo images with Images.LoadPoster, converter should do it
8) Optimizations, lists and showmovie/series is pretty slow, maybe load before showing? 